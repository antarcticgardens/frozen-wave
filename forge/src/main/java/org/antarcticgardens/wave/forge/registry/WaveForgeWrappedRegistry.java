package org.antarcticgardens.wave.forge.registry;

import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.util.Identifier;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import org.antarcticgardens.wave.registry.WaveRegistry;

import java.util.function.Supplier;

public class WaveForgeWrappedRegistry<T> extends WaveRegistry<T> {
    private final String modId;
    private final DeferredRegister<T> deferredRegister;
    private final Registry<T> registry;
    
    WaveForgeWrappedRegistry(String modId, RegistryKey<? extends Registry<T>> registryKey) {
        this.modId = modId;
        
        deferredRegister = DeferredRegister.create(registryKey, modId);
        deferredRegister.register(FMLJavaModLoadingContext.get().getModEventBus());
        
        registry = (Registry<T>) Registries.REGISTRIES.get(registryKey.getValue());
    }

    @Override
    public void register(String key, Supplier<? extends T> entry) {
        deferredRegister.register(key, entry);
    }

    @Override
    public T get(String key) {
        return registry.get(new Identifier(modId, key));
    }
}
