package org.antarcticgardens.wave.forge;

import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.antarcticgardens.wave.Wave;
import net.minecraftforge.fml.common.Mod;
import org.antarcticgardens.wave.block.blockentity.WaveBlockEntityTypeBuilder;
import org.antarcticgardens.wave.forge.block.blockentity.WaveForgeBlockEntityTypeBuilder;
import org.antarcticgardens.wave.WaveImplementations;
import org.antarcticgardens.wave.forge.network.packet.WaveForgePacketSender;
import org.antarcticgardens.wave.forge.registry.*;
import org.antarcticgardens.wave.network.packet.WavePacketSender;
import org.antarcticgardens.wave.registry.*;

import java.util.Map;

@Mod(Wave.MOD_ID)
public class WaveForge {
    private static volatile boolean ready = false;
    
    public WaveForge() {
        WaveImplementations.put(Map.of(
                WaveRegistries.class, new WaveForgeRegistries(),
                WaveTabRegistry.class, new WaveForgeTabRegistry(),
                WaveC2SPacketRegistry.class, new WaveForgeC2SPacketRegistry(),

                WavePacketSender.class, new WaveForgePacketSender(),

                WaveBlockEntityTypeBuilder.class, new WaveForgeBlockEntityTypeBuilder()
        ));

        FMLJavaModLoadingContext.get().getModEventBus().addListener(WaveForgeClient::onClientSetup);
        
        Wave.init();
        ready = true;
    }

    /**
     * In Forge, mod instance construction is async so a dependent mod may construct earlier than Wave.
     * But we are in no hurry
     * @param runnable something to run
     */
    public static void runWhenReady(Runnable runnable) {
        while (!ready) {
            Thread.onSpinWait();
        }
        
        runnable.run();
    }
}