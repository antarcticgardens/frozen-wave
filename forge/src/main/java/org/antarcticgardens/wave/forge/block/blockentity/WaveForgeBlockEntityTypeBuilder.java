package org.antarcticgardens.wave.forge.block.blockentity;

import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import org.antarcticgardens.wave.block.blockentity.WaveBlockEntityTypeBuilder;

@SuppressWarnings("unchecked")
public class WaveForgeBlockEntityTypeBuilder extends WaveBlockEntityTypeBuilder {
    @Override
    public <T extends BlockEntity> BlockEntityType<T> buildImpl(Factory<? extends T> blockEntityFactory, Block... validBlocks) {
        return (BlockEntityType<T>) BlockEntityType.Builder.create(blockEntityFactory::create, validBlocks).build(null);
    }
}
