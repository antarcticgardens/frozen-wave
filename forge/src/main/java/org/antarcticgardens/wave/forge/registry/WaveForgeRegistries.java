package org.antarcticgardens.wave.forge.registry;

import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import org.antarcticgardens.wave.registry.WaveRegistries;
import org.antarcticgardens.wave.registry.WaveRegistry;

public class WaveForgeRegistries extends WaveRegistries {
    @Override
    protected <T> WaveRegistry<T> wrapMinecraftRegistryImpl(String modId, RegistryKey<? extends Registry<T>> registryKey) {
        return new WaveForgeWrappedRegistry<>(modId, registryKey);
    }
}
