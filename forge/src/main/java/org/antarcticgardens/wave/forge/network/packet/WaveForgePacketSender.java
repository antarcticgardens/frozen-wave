package org.antarcticgardens.wave.forge.network.packet;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.c2s.play.CustomPayloadC2SPacket;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.network.packet.WavePacketSender;

public class WaveForgePacketSender extends WavePacketSender {
    @Override
    public void sendS2CImpl(ServerPlayerEntity player, Identifier identifier, PacketByteBuf packet) {
        player.networkHandler.sendPacket(new CustomPayloadS2CPacket(identifier, packet));
    }

    @Override
    public void sendC2SImpl(Identifier identifier, PacketByteBuf packet) {
        ClientPlayNetworkHandler networkHandler = MinecraftClient.getInstance().getNetworkHandler();
        
        if (networkHandler == null) {
            throw new RuntimeException("SOMEHOW we don't have a client network handler right now");
        }
        
        networkHandler.sendPacket(new CustomPayloadC2SPacket(identifier, packet));
    }
}
