package org.antarcticgardens.wave.forge.registry;

import net.minecraft.util.Identifier;
import net.minecraftforge.network.NetworkInstance;
import net.minecraftforge.network.NetworkRegistry;
import org.antarcticgardens.wave.forge.network.ForgeNetworkHelper;
import org.antarcticgardens.wave.network.packet.IC2SPacketHandler;
import org.antarcticgardens.wave.registry.WaveC2SPacketRegistry;

public class WaveForgeC2SPacketRegistry extends WaveC2SPacketRegistry {
    @Override
    public void registerImpl(Identifier identifier, IC2SPacketHandler handler) {
        ForgeNetworkHelper.registerC2SPacket(identifier, handler);
    }
}
