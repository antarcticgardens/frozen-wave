package org.antarcticgardens.wave.forge.registry;

import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.forge.network.ForgeNetworkHelper;
import org.antarcticgardens.wave.network.packet.IS2CPacketHandler;
import org.antarcticgardens.wave.registry.WaveS2CPacketRegistry;

public class WaveForgeS2CPacketRegistry extends WaveS2CPacketRegistry {
    @Override
    public void registerImpl(Identifier identifier, IS2CPacketHandler handler) {
        ForgeNetworkHelper.registerS2CPacket(identifier, handler);
    }
}
