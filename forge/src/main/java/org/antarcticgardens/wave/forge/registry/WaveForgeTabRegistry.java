package org.antarcticgardens.wave.forge.registry;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.registry.Registries;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import org.antarcticgardens.wave.inventory.tab.TabConfiguration;
import org.antarcticgardens.wave.util.WaveCachedSupplier;
import org.antarcticgardens.wave.registry.WaveTabRegistry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WaveForgeTabRegistry extends WaveTabRegistry {
    private final Map<String, DeferredRegister<ItemGroup>> registries = new HashMap<>();

    @Override
    public void registerImpl(Identifier identifier, List<WaveCachedSupplier<? extends Item>> items, TabConfiguration config) {
        getOrCreateDeferredRegister(identifier.getNamespace()).register(identifier.getPath(), () -> 
                ItemGroup.builder()
                        .icon(() -> config.getIcon(null))
                        .displayName(Text.translatable("itemGroup." + identifier.getNamespace() + "." + identifier.getPath()))
                        .entries((context, entries) -> {
                            for (WaveCachedSupplier<? extends Item> i : items) {
                                entries.add(i.get().getDefaultStack());
                            }
                        })
                        .build());
    }

    private DeferredRegister<ItemGroup> getOrCreateDeferredRegister(String namespace) {
        DeferredRegister<ItemGroup> dr = registries.get(namespace);
        
        if (dr == null) {
            dr = DeferredRegister.create(Registries.ITEM_GROUP.getKey(), namespace);
            registries.put(namespace, dr);
            dr.register(FMLJavaModLoadingContext.get().getModEventBus());
        }
        
        return dr;
    }
}
