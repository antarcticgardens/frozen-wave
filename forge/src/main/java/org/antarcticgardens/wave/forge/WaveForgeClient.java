package org.antarcticgardens.wave.forge;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import org.antarcticgardens.wave.forge.registry.WaveForgeS2CPacketRegistry;
import org.antarcticgardens.wave.WaveImplementations;
import org.antarcticgardens.wave.forge.registry.WaveForgeShaderRegistry;
import org.antarcticgardens.wave.registry.WaveS2CPacketRegistry;
import org.antarcticgardens.wave.registry.WaveShaderRegistry;

import java.util.Map;

public class WaveForgeClient {
    public static void onClientSetup(final FMLClientSetupEvent event) {
        WaveImplementations.put(Map.of(
                WaveS2CPacketRegistry.class, new WaveForgeS2CPacketRegistry(),
                WaveShaderRegistry.class, new WaveForgeShaderRegistry() 
        ));
    }
}
