package org.antarcticgardens.wave.forge.network;

import net.minecraft.util.Identifier;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.network.NetworkInstance;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.event.EventNetworkChannel;
import org.antarcticgardens.wave.network.packet.IC2SPacketHandler;
import org.antarcticgardens.wave.network.packet.IS2CPacketHandler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ForgeNetworkHelper {
    public static void registerC2SPacket(Identifier identifier, IC2SPacketHandler handler) {
        createChannel(identifier).addListener(event -> {
            if (event instanceof NetworkEvent.ClientCustomPayloadEvent) {
                NetworkEvent.Context ctx = event.getSource().get();
                ctx.enqueueWork(() -> handler.handle(ctx.getSender(), event.getPayload()));
                ctx.setPacketHandled(true);
            }
        });
    }
    
    public static void registerS2CPacket(Identifier identifier, IS2CPacketHandler handler) {
        createChannel(identifier).addListener(event -> {
            if (event instanceof NetworkEvent.ServerCustomPayloadEvent) {
                NetworkEvent.Context ctx = event.getSource().get();
                ctx.enqueueWork(() -> DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> handler.handle(event.getPayload())));
                ctx.setPacketHandled(true);
            }
        });
    }
    
    private static EventNetworkChannel createChannel(Identifier identifier) {
        return NetworkRegistry.newEventChannel(identifier, () -> "1", "1"::equals, "1"::equals);
    }
}
