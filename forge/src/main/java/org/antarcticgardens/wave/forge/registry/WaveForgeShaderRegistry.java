package org.antarcticgardens.wave.forge.registry;

import net.minecraft.client.gl.ShaderProgram;
import net.minecraft.client.render.VertexFormat;
import net.minecraft.util.Identifier;
import net.minecraftforge.client.event.RegisterShadersEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.antarcticgardens.wave.registry.WaveShaderRegistry;

import java.io.IOException;
import java.util.function.Consumer;

public class WaveForgeShaderRegistry extends WaveShaderRegistry {
    @Override
    public void registerImpl(Identifier identifier, VertexFormat vertexFormat, Consumer<ShaderProgram> consumer) {
        FMLJavaModLoadingContext.get().getModEventBus().addListener((Consumer<RegisterShadersEvent>) event -> {
            try {
                event.registerShader(new ShaderProgram(event.getResourceProvider(), identifier, vertexFormat), consumer);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
