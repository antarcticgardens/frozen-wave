package org.antarcticgardens.wave.fabric.registry;

import net.fabricmc.fabric.api.client.rendering.v1.CoreShaderRegistrationCallback;
import net.minecraft.client.gl.ShaderProgram;
import net.minecraft.client.render.VertexFormat;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.registry.WaveShaderRegistry;

import java.util.function.Consumer;

public class WaveFabricShaderRegistry extends WaveShaderRegistry {
    @Override
    public void registerImpl(Identifier identifier, VertexFormat vertexFormat, Consumer<ShaderProgram> consumer) {
        CoreShaderRegistrationCallback.EVENT.register(ctx -> {
            ctx.register(identifier, vertexFormat, consumer);
        });
    }
}
