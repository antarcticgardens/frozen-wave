package org.antarcticgardens.wave.fabric.registry;

import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.registry.WaveRegistry;

import java.util.function.Supplier;

public class WaveFabricWrappedRegistry<T> extends WaveRegistry<T> {
    private final String modId;
    private final Registry<T> registry;
    
    WaveFabricWrappedRegistry(String modId, RegistryKey<? extends Registry<T>> registryKey) {
        this.modId = modId;
        registry = (Registry<T>) Registries.REGISTRIES.get(registryKey.getValue());
    }

    @Override
    public void register(String key, Supplier<? extends T> entry) {
        Registry.register(registry, new Identifier(modId, key), entry.get());
    }

    @Override
    public T get(String key) {
        return registry.get(new Identifier(modId, key));
    }
}
