package org.antarcticgardens.wave.fabric.network.packet;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.network.packet.WavePacketSender;

public class WaveFabricPacketSender extends WavePacketSender {
    @Override
    public void sendS2CImpl(ServerPlayerEntity player, Identifier identifier, PacketByteBuf packet) {
        ServerPlayNetworking.send(player, identifier, packet);
    }

    @Override
    public void sendC2SImpl(Identifier identifier, PacketByteBuf packet) {
        ClientPlayNetworking.send(identifier, packet);
    }
}
