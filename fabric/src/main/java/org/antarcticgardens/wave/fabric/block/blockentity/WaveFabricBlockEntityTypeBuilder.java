package org.antarcticgardens.wave.fabric.block.blockentity;

import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import org.antarcticgardens.wave.block.blockentity.WaveBlockEntityTypeBuilder;

@SuppressWarnings("unchecked")
public class WaveFabricBlockEntityTypeBuilder extends WaveBlockEntityTypeBuilder {
    @Override
    public <T extends BlockEntity> BlockEntityType<T> buildImpl(Factory<? extends T> blockEntityFactory, Block... validBlocks) {
        return (BlockEntityType<T>) FabricBlockEntityTypeBuilder.create(blockEntityFactory::create, validBlocks).build();
    }
}
