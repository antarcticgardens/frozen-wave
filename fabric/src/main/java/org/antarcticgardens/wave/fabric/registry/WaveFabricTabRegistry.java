package org.antarcticgardens.wave.fabric.registry;

import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.inventory.tab.TabConfiguration;
import org.antarcticgardens.wave.util.WaveCachedSupplier;
import org.antarcticgardens.wave.registry.WaveTabRegistry;

import java.util.List;

public class WaveFabricTabRegistry extends WaveTabRegistry {
    @Override
    public void registerImpl(Identifier identifier, List<WaveCachedSupplier<? extends Item>> items, TabConfiguration config) {
        Registry.register(Registries.ITEM_GROUP, identifier, FabricItemGroup.builder()
                .icon(() -> config.getIcon(null))
                .displayName(Text.translatable("itemGroup." + identifier.getNamespace() + "." + identifier.getPath()))
                .entries((context, entries) -> {
                    for (WaveCachedSupplier<? extends Item> i : items) {
                        entries.add(i.get().getDefaultStack());
                    }
                })
                .build());
    }
}
