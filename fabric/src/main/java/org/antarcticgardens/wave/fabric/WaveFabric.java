package org.antarcticgardens.wave.fabric;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import org.antarcticgardens.wave.Wave;
import org.antarcticgardens.wave.block.blockentity.WaveBlockEntityTypeBuilder;
import org.antarcticgardens.wave.fabric.block.blockentity.WaveFabricBlockEntityTypeBuilder;
import org.antarcticgardens.wave.WaveImplementations;
import org.antarcticgardens.wave.fabric.network.packet.WaveFabricPacketSender;
import org.antarcticgardens.wave.fabric.registry.*;
import org.antarcticgardens.wave.network.packet.WavePacketSender;
import org.antarcticgardens.wave.registry.*;

import java.util.Map;

public class WaveFabric implements ModInitializer {
    @Override
    public void onInitialize() {
        WaveImplementations.put(Map.of(
                WaveRegistries.class, new WaveFabricRegistries(),
                WaveTabRegistry.class, new WaveFabricTabRegistry(),
                WaveC2SPacketRegistry.class, new WaveFabricC2SPacketRegistry(),

                WavePacketSender.class, new WaveFabricPacketSender(),
                
                WaveBlockEntityTypeBuilder.class, new WaveFabricBlockEntityTypeBuilder()
        ));

        Wave.init();

        for (Runnable entrypoint : FabricLoader.getInstance().getEntrypoints("wave-main", Runnable.class)) {
            entrypoint.run();
        }
    }
}