package org.antarcticgardens.wave.fabric.registry;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.network.packet.IS2CPacketHandler;
import org.antarcticgardens.wave.registry.WaveS2CPacketRegistry;

public class WaveFabricS2CPacketRegistry extends WaveS2CPacketRegistry {
    @Override
    public void registerImpl(Identifier identifier, IS2CPacketHandler handler) {
        ClientPlayNetworking.registerGlobalReceiver(identifier, (client, handler1, buf, responseSender) -> {
            MinecraftClient.getInstance().execute(() -> handler.handle(buf)); // TODO: make it run on server tick
        });
    }
}
