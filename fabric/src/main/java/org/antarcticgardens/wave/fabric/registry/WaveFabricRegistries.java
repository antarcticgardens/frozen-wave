package org.antarcticgardens.wave.fabric.registry;

import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import org.antarcticgardens.wave.registry.WaveRegistries;
import org.antarcticgardens.wave.registry.WaveRegistry;

public class WaveFabricRegistries extends WaveRegistries {
    @Override
    protected <T> WaveRegistry<T> wrapMinecraftRegistryImpl(String modId, RegistryKey<? extends Registry<T>> registryKey) {
        return new WaveFabricWrappedRegistry<>(modId, registryKey);
    }
}
