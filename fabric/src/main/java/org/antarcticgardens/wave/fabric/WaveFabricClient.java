package org.antarcticgardens.wave.fabric;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import org.antarcticgardens.wave.fabric.registry.WaveFabricS2CPacketRegistry;
import org.antarcticgardens.wave.fabric.registry.WaveFabricShaderRegistry;
import org.antarcticgardens.wave.WaveImplementations;
import org.antarcticgardens.wave.registry.WaveS2CPacketRegistry;
import org.antarcticgardens.wave.registry.WaveShaderRegistry;

import java.util.Map;

public class WaveFabricClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        WaveImplementations.put(Map.of(
                WaveS2CPacketRegistry.class, new WaveFabricS2CPacketRegistry(),
                WaveShaderRegistry.class, new WaveFabricShaderRegistry()
        ));

        for (Runnable entrypoint : FabricLoader.getInstance().getEntrypoints("wave-client", Runnable.class)) {
            entrypoint.run();
        }
    }
}
