package org.antarcticgardens.wave.fabric.registry;

import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.network.packet.IC2SPacketHandler;
import org.antarcticgardens.wave.registry.WaveC2SPacketRegistry;

public class WaveFabricC2SPacketRegistry extends WaveC2SPacketRegistry {
    @Override
    public void registerImpl(Identifier identifier, IC2SPacketHandler handler) {
        ServerPlayNetworking.registerGlobalReceiver(identifier, ((server, player, handler1, buf, responseSender) -> {
            handler.handle(player, buf);
        }));
    }
}
