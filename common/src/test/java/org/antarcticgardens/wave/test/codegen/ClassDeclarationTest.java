package org.antarcticgardens.wave.test.codegen;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.declarations.ClassDeclarationElement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Modifier;

public class ClassDeclarationTest {
    private JavaWriter writer;
    
    @BeforeEach
    void setup() {
        writer = new JavaWriter(" ".repeat(4));
    }
    
    @Test
    void plainClass() {
        ClassDeclarationElement clazz = new ClassDeclarationElement("PlainJavaClass");
        clazz.generate(writer);

        Assertions.assertEquals("""
                class PlainJavaClass {
                }
                """, writer.toString());
    }

    @Test
    void classWithModifiers() {
        ClassDeclarationElement clazz = new ClassDeclarationElement("ClassWithModifiers")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL);

        clazz.generate(writer);

        Assertions.assertEquals("""
                public final class ClassWithModifiers {
                }
                """, writer.toString());
    }

    @Test
    void classWithGeneric() {
        ClassDeclarationElement clazz = new ClassDeclarationElement("ClassWithGeneric")
                .addModifiers(Modifier.PUBLIC)
                .addGenerics("T");

        clazz.generate(writer);

        Assertions.assertEquals("""
                public class ClassWithGeneric<T> {
                }
                """, writer.toString());
    }

    @Test
    void classWithExtends() {
        ClassDeclarationElement clazz = new ClassDeclarationElement("ClassWithExtends")
                .addModifiers(Modifier.PUBLIC)
                .extend("SomeSuperclass");

        clazz.generate(writer);

        Assertions.assertEquals("""
                public class ClassWithExtends extends SomeSuperclass {
                }
                """, writer.toString());
    }

    @Test
    void classWithGenericsAndImplements() {
        ClassDeclarationElement clazz = new ClassDeclarationElement("ClassWithGenericsAndImplements")
                .addModifiers(Modifier.PRIVATE)
                .addImplements("Runnable", "Iterable")
                .addGenerics("K", "V");

        clazz.generate(writer);

        Assertions.assertEquals("""
                private class ClassWithGenericsAndImplements<K, V> implements Runnable, Iterable {
                }
                """, writer.toString());
    }

    @Test
    void superUltraMegaClass() {
        ClassDeclarationElement clazz = new ClassDeclarationElement("SuperUltraMegaClass")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                .extend("MegaClass")
                .addImplements("Runnable", "Iterable")
                .addGenerics("K", "V", "E", "T");

        clazz.generate(writer);

        Assertions.assertEquals("""
                public static final class SuperUltraMegaClass<K, V, E, T> extends MegaClass implements Runnable, Iterable {
                }
                """, writer.toString());
    }
}
