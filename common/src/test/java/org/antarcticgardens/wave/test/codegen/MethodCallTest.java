package org.antarcticgardens.wave.test.codegen;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MethodCallTest {
    private JavaWriter writer;

    @BeforeEach
    void setup() {
        writer = new JavaWriter(" ".repeat(4));
    }

    @Test
    void test1() {
        MethodCallElement call = new MethodCallElement("object", "method");
        call.generate(writer);

        Assertions.assertEquals("""
                object.method();
                """, writer.toString());
    }

    @Test
    void test2() {
        MethodCallElement call = new MethodCallElement("System.out", "println")
                .addArguments("\"Hello, World!\"");
        call.generate(writer);

        Assertions.assertEquals("""
                System.out.println("Hello, World!");
                """, writer.toString());
    }
}
