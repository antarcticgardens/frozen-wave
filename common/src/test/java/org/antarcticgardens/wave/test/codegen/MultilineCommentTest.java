package org.antarcticgardens.wave.test.codegen;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.MultilineCommentElement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MultilineCommentTest {
    private JavaWriter writer;
    private MultilineCommentElement comment;
    
    @BeforeEach
    void setup() {
        writer = new JavaWriter(" ".repeat(4));
        comment = new MultilineCommentElement();
    }
    
    @Test
    void test1() {
        comment.addLine("Some text");
        comment.generate(writer);

        Assertions.assertEquals("""
                /*
                    Some text
                 */
                """, writer.toString());
    }

    @Test
    void test2() {
        comment.addLine("Lorem ipsum")
                .addLine("dolor sit amet");

        comment.generate(writer);

        Assertions.assertEquals("""
                /*
                    Lorem ipsum
                    dolor sit amet
                 */
                """, writer.toString());
    }
}
