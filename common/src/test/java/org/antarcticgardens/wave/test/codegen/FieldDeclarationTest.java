package org.antarcticgardens.wave.test.codegen;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.declarations.FieldDeclarationElement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Modifier;

public class FieldDeclarationTest {
    private JavaWriter writer;

    @BeforeEach
    void setup() {
        writer = new JavaWriter(" ".repeat(4));
    }

    @Test
    void plainField() {
        FieldDeclarationElement field = new FieldDeclarationElement("int", "i");
        field.generate(writer);
        
        Assertions.assertEquals("""
                int i;
                """, writer.toString());
    }

    @Test
    void fieldWithModifiers() {
        FieldDeclarationElement field = new FieldDeclarationElement("int", "i")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC);
        
        field.generate(writer);

        Assertions.assertEquals("""
                public static int i;
                """, writer.toString());
    }

    @Test
    void fieldWithValue() {
        FieldDeclarationElement field = new FieldDeclarationElement("int", "i")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .setValue("20");
        
        field.generate(writer);

        Assertions.assertEquals("""
                public final int i = 20;
                """, writer.toString());
    }
    
    @Test
    void fieldOfTypeWithGenerics() {
        FieldDeclarationElement field = new FieldDeclarationElement("List", "strings")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addGenerics("String");

        field.generate(writer);

        Assertions.assertEquals("""
                public final List<String> strings;
                """, writer.toString());
    }
}
