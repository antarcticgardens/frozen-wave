package org.antarcticgardens.wave.test.codegen;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.declarations.ClassDeclarationElement;
import org.antarcticgardens.wave.codegen.elements.declarations.MethodDeclarationElement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Modifier;

public class MethodDeclarationTest {
    private JavaWriter writer;
    private ClassDeclarationElement clazz;

    @BeforeEach
    void setup() {
        writer = new JavaWriter(" ".repeat(4));
        clazz = new ClassDeclarationElement("ContainerClass")
                .addModifiers(Modifier.PUBLIC);
    }

    @Test
    void plainMethod() {
        MethodDeclarationElement method = new MethodDeclarationElement("run", "void");
        
        clazz.addElement(0, method);
        clazz.generate(writer);

        Assertions.assertEquals("""
                public class ContainerClass {
                    void run() {
                    }
                }
                """, writer.toString());
    }

    @Test
    void methodWithModifiersAndArguments() {
        MethodDeclarationElement method = new MethodDeclarationElement("doSomething", "List<Float>")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addArgument("String", "str")
                .addArgument("int", "i");

        clazz.addElement(0, method);
        clazz.generate(writer);

        Assertions.assertEquals("""
                public class ContainerClass {
                    public static List<Float> doSomething(String str, int i) {
                    }
                }
                """, writer.toString());
    }

    @Test
    void methodWithGenerics() {
        MethodDeclarationElement method = new MethodDeclarationElement("doSomething", "List<G>")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .addGenerics("T", "G")
                .addArgument("T[]", "a")
                .addArgument("Function<T, G>", "f");

        clazz.addElement(0, method);
        clazz.generate(writer);

        Assertions.assertEquals("""
                public class ContainerClass {
                    public static <T, G> List<G> doSomething(T[] a, Function<T, G> f) {
                    }
                }
                """, writer.toString());
    }
}
