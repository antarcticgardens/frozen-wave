package org.antarcticgardens.wave.i18n;

import com.google.gson.Gson;
import org.antarcticgardens.wave.datagen.WaveDataGen;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

public class TranslationHelper {

    /**
     * Custom translation key -> vanilla translation key
     */
    private static final Map<String, String> translationKeys = new HashMap<>();

    /**
     * code -> (vanilla translation key -> value)
     */
    private static final Map<String, Map<String, String>> translationOverrides = new HashMap<>();

    public static final String IGNORE_FOLDERS_STARTING_WITH = "_";

    public static void addKey(String key, String vanilla) {
        translationKeys.put(key, vanilla);
    }
    public static void addDefault(String key, String value) {
        translationOverrides.computeIfAbsent("en_us", (a) -> new HashMap<>()).put(translationKeys.get(key), value);
    }

    public static void bakeTranslations() {
        Map<String, Map<String, String>> translations = new HashMap<>();

        File translationsFolder = new File(WaveDataGen.ROOT, "./translations");
        translationsFolder.mkdirs();

        for (File folder : translationsFolder.listFiles()) {
            if (!folder.getName().startsWith(IGNORE_FOLDERS_STARTING_WITH)) {
                Map<String, String> translation = new HashMap<>();
                StringBuilder outLog = new StringBuilder();

                try {
                    Files.walkFileTree(folder.toPath(), new SimpleFileVisitor<>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                            if (file.endsWith(".csv") && attrs.isRegularFile()) {
                                String content = Files.readString(file);
                                String[] rows = content.split("\n");

                                for (String row : rows) {
                                    if (!row.startsWith("_") && !row.startsWith(".")) {
                                        row = row.replaceAll("(?<!\\\\)\"", "");
                                        String[] trans = row.split(",", 1);
                                        String key = translationKeys.get(trans[0]);
                                        if (key != null) {
                                            translation.put(key, trans[1]);
                                        } else {
                                            outLog.append("[Unidentified key] -> ").append(trans[0]).append("\n");
                                        }
                                    }
                                }

                            }
                            return super.visitFile(file, attrs);
                        }
                    });
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                File errorLog = new File(translationsFolder,  "_info/problems/" + folder.getName() + ".log");
                errorLog.getParentFile().mkdirs();

                if (!outLog.isEmpty()) {
                    try {
                        Files.write(errorLog.toPath(), outLog.toString().getBytes());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }

                translations.put(folder.getName(), translation);
            }
        }

        for (Map.Entry<String, Map<String, String>> overrides : translationOverrides.entrySet()) {
            Map<String, String> map = translations.computeIfAbsent(overrides.getKey(), (v) -> new HashMap<>());
            map.putAll(overrides.getValue());
        }

        File baseTranslationFolder = new File(WaveDataGen.DIRECTORY, "resources/assets/" + WaveDataGen.modID + "/lang");
        baseTranslationFolder.mkdirs();

        for (Map.Entry<String, Map<String, String>> translation : translations.entrySet()) {
            try {
                Files.write(new File(baseTranslationFolder, translation.getKey() + ".json").toPath(),
                        new Gson().toJson(translation.getValue()).getBytes());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }

    static {
        WaveDataGen.runAfterDone(TranslationHelper::bakeTranslations, WaveDataGen.Priority.TRANSLATION);
    }
}
