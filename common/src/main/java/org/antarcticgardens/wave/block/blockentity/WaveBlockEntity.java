package org.antarcticgardens.wave.block.blockentity;

import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.Pair;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.antarcticgardens.wave.datagen.annotation.ClassAnnotationProcessor;
import org.antarcticgardens.wave.block.BlocksFileGenerator;
import org.antarcticgardens.wave.block.WaveBlock;
import org.antarcticgardens.wave.codegen.reference.ReferenceUtils;
import org.antarcticgardens.wave.codegen.elements.declarations.FieldDeclarationElement;
import org.antarcticgardens.wave.codegen.reference.ReferenceContext;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.util.WaveCachedSupplier;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.slf4j.Logger;

import javax.lang.model.element.Modifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WaveBlockEntity {
    String value();
    String[] validBlocks() default {};
    
    class Processor implements ClassAnnotationProcessor<WaveBlockEntity> {
        private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
        private static final Map<String, Pair<Class<?>, List<String>>> blockEntities = new HashMap<>();
        
        static {
            BlockEntityTypesFileGenerator.setup();
            WaveDataGen.runAfterDone(Processor::postProcess, WaveDataGen.Priority.BLOCK_ENTITY);
        }
        
        private static void postProcess() {
            for (Map.Entry<String, Pair<Class<?>, List<String>>> e : blockEntities.entrySet()) {
                String id = e.getKey();
                Class<?> aClass = e.getValue().getLeft();
                List<String> validBlocks = e.getValue().getRight();

                ReferenceContext ref = BlockEntityTypesFileGenerator.BUILDER.getReferenceContext();
                
                StringBuilder validBlocksStringBuilder = new StringBuilder();
                
                for (int i = 0; i < validBlocks.size(); i++) {
                    String block = validBlocks.get(i);
                    if (!WaveBlock.Processor.blockExists(block)) {
                        LOGGER.error("Couldn't find a block with id {}", block);
                        continue;
                    }
                    
                    validBlocksStringBuilder.append(ref.getReference(BlocksFileGenerator.BUILDER)).append(".").append(block.toUpperCase()).append(".get()");
                    
                    if (i != validBlocks.size() - 1) {
                        validBlocksStringBuilder.append(", ");
                    }
                }

                FieldDeclarationElement blockEntityTypeField = new FieldDeclarationElement(ReferenceUtils.format("{0}<{1}<{2}>>", ref, 
                                WaveCachedSupplier.class, BlockEntityType.class, aClass), id.toUpperCase())
                        .addModifiers(Modifier.PROTECTED, Modifier.STATIC, Modifier.FINAL)
                        .setValue(ReferenceUtils.format("new {0}<>(() -> {1}.build({2}::new, {3}))", ref,
                                WaveCachedSupplier.class, WaveBlockEntityTypeBuilder.class, aClass, validBlocksStringBuilder.toString()));

                MethodCallElement registerCall = new MethodCallElement(BlocksFileGenerator.REGISTRY.getName(), "register")
                        .addArguments("\"" + id + "\"", id.toUpperCase());

                BlockEntityTypesFileGenerator.BUILDER.addElement(0, blockEntityTypeField);
                BlockEntityTypesFileGenerator.METHOD.addElement(0, registerCall);
            }
        }
        
        @Override
        public void process(Class<?> aClass, WaveBlockEntity annotation) {
            if (blockEntities.containsKey(annotation.value())) {
                LOGGER.error("Trying to add block entity {} with id {} while it is already taken by {}",
                        aClass.getName(), annotation.value(), blockEntities.get(annotation.value()).getLeft().getName());
                return;
            }

            List<String> validBlocks = new ArrayList<>(List.of(annotation.validBlocks()));
            Class<?> parent = aClass.getDeclaringClass();

            if (parent != null) {
                WaveBlock b = parent.getAnnotation(WaveBlock.class);
                if (b != null && !validBlocks.contains(b.value())) {
                    validBlocks.add(b.value());
                }
            }
            
            if (validBlocks.isEmpty()) {
                LOGGER.error("No valid blocks were specified for {} and it is not in a block class", aClass.getName());
                return;
            }
            
            blockEntities.put(annotation.value(), new Pair<>(aClass, validBlocks));
        }
    }
}
