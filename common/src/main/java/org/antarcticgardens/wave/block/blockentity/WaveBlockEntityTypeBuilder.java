package org.antarcticgardens.wave.block.blockentity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import org.antarcticgardens.wave.WaveImplementations;

public abstract class WaveBlockEntityTypeBuilder {
    public static <T extends BlockEntity> BlockEntityType<T> build(Factory<? extends T> blockEntityFactory, Block... validBlocks) {
        return WaveImplementations.get(WaveBlockEntityTypeBuilder.class).buildImpl(blockEntityFactory, validBlocks);
    }
    
    public abstract <T extends BlockEntity> BlockEntityType<T> buildImpl(Factory<? extends T> blockEntityFactory, Block... validBlocks);

    public interface Factory<T extends BlockEntity> {
        T create(BlockPos blockPos, BlockState blockState);
    }
}
