package org.antarcticgardens.wave.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.antarcticgardens.wave.datagen.annotation.ClassAnnotationProcessor;
import org.antarcticgardens.wave.codegen.reference.ReferenceUtils;
import org.antarcticgardens.wave.codegen.elements.declarations.FieldDeclarationElement;
import org.antarcticgardens.wave.codegen.reference.ReferenceContext;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.i18n.DefaultTranslation;
import org.antarcticgardens.wave.i18n.TranslationHelper;
import org.antarcticgardens.wave.item.WaveItem;
import org.antarcticgardens.wave.util.WaveCachedSupplier;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.slf4j.Logger;

import javax.lang.model.element.Modifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WaveBlock {
    String value();
    
    class Processor implements ClassAnnotationProcessor<WaveBlock> {
        private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
        private static final Map<String, Class<?>> blocks = new HashMap<>();
        
        static {
            BlocksFileGenerator.setup();
            WaveDataGen.runAfterDone(Processor::postProcess, WaveDataGen.Priority.BLOCK);
        }
        
        private static void postProcess() {
            for (Map.Entry<String, Class<?>> e : blocks.entrySet()) {
                String id = e.getKey();
                Class<?> aClass = e.getValue();
                
                WaveItem.Processor.addItem(id, aClass);

                ReferenceContext ref = BlocksFileGenerator.BUILDER.getReferenceContext();
                
                FieldDeclarationElement blockField = new FieldDeclarationElement(ref.getReference(WaveCachedSupplier.class), id.toUpperCase())
                        .addModifiers(Modifier.PROTECTED, Modifier.STATIC, Modifier.FINAL)
                        .addGenerics(ref.getReference(aClass))
                        .setValue(ReferenceUtils.format("new {0}<>(() -> new {1}({2}.create()))", ref, 
                                WaveCachedSupplier.class, aClass, AbstractBlock.Settings.class));
                
                MethodCallElement registerCall = new MethodCallElement(BlocksFileGenerator.REGISTRY.getName(), "register")
                        .addArguments("\"" + id + "\"", id.toUpperCase());

                BlocksFileGenerator.BUILDER.addElement(0, blockField);
                BlocksFileGenerator.METHOD.addElement(0, registerCall);

                TranslationHelper.addKey("block." + id, String.format("block.%s.%s", WaveDataGen.modID, id));

                DefaultTranslation defaultTranslation = aClass.getAnnotation(DefaultTranslation.class);
                if (defaultTranslation != null) {
                    TranslationHelper.addDefault("block." + id, defaultTranslation.value());
                }
            }
        }
        
        public static boolean blockExists(String name) {
            return blocks.containsKey(name);
        }
        
        public static Class<?> getBlockClass(String name) {
            return blocks.get(name);
        }
        
        public static Map<String, Class<?>> getBlocks() {
            return Collections.unmodifiableMap(blocks);
        }
        
        @Override
        public void process(Class<?> aClass, WaveBlock annotation) {
            if (!Block.class.isAssignableFrom(aClass)) {
                LOGGER.error("Class {} annotated with @{} does not extend Block class", aClass.getName(), WaveBlock.class.getSimpleName());
                return;
            }
            
            if (blockExists(annotation.value())) {
                LOGGER.error("Trying to add block {} with id {} while it is already taken by {}",
                        aClass.getName(), annotation.value(), blocks.get(annotation.value()).getName());
                return;
            }

            blocks.put(annotation.value(), aClass);
        }
    }
}
