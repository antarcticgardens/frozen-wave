package org.antarcticgardens.wave.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import org.antarcticgardens.wave.datagen.annotation.ClassAnnotationProcessor;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.datagen.asset.BlockAssetGenerator;
import org.antarcticgardens.wave.datagen.asset.blockstate.SimpleBlockStateGenerator;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.slf4j.Logger;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WaveBlockAssetGenerator {
    String value() default "";

    class Processor implements ClassAnnotationProcessor<WaveBlockAssetGenerator> {
        private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
        private static final Map<String, Class<?>> generators = new HashMap<>();

        static {
            WaveDataGen.runAfterDone(WaveBlockAssetGenerator.Processor::postProcess, WaveDataGen.Priority.BLOCK_ASSETS);
            SimpleBlockStateGenerator.setup();
        }

        private static void postProcess() {
            for (Map.Entry<String, Class<?>> e : generators.entrySet()) {
                String id = e.getKey();
                Class<?> aClass = e.getValue();
                Class<?> blockClass = WaveBlock.Processor.getBlockClass(id);
                
                if (!BlockAssetGenerator.class.isAssignableFrom(aClass)) {
                    LOGGER.error("{} does not implement {}", 
                            aClass.getName(), BlockAssetGenerator.class.getSimpleName());
                    continue;
                }
                
                try {
                    Constructor<?> blockConstructor = blockClass.getConstructor(AbstractBlock.Settings.class);
                    Block block = (Block) blockConstructor.newInstance(AbstractBlock.Settings.create());

                    Constructor<?> generatorConstructor = aClass.getDeclaredConstructor();
                    BlockAssetGenerator generator = (BlockAssetGenerator) generatorConstructor.newInstance();
                    
                    generator.generateAssets(block);
                } catch (NoSuchMethodException | InstantiationException | IllegalAccessException |
                         InvocationTargetException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
        
        public static boolean isAssetGeneratorPresent(String id) {
            return generators.containsKey(id);
        }

        @Override
        public void process(Class<?> aClass, WaveBlockAssetGenerator annotation) {
            if (generators.containsKey(annotation.value())) {
                LOGGER.error("Trying to add asset generator {} for block {} while it is already taken by {}",
                        aClass.getName(), annotation.value(), generators.get(annotation.value()).getName());
                return;
            }
            
            String block = annotation.value();
            
            if (block.isEmpty()) {
                Class<?> parent = aClass.getDeclaringClass();
                WaveBlock wb;
                
                if (parent != null && (wb = parent.getAnnotation(WaveBlock.class)) != null) {
                    block = wb.value();
                } else {
                    LOGGER.error("Target block is not specified for {} and it is not in a block class", aClass.getName());
                    return;
                }
            }

            if (!WaveBlock.Processor.blockExists(block)) { 
                LOGGER.error("Couldn't find a block with id {}", block);
                return;
            }

            generators.put(block, aClass);
        }
    }
}
