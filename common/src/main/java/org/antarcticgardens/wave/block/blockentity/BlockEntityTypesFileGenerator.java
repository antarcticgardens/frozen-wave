package org.antarcticgardens.wave.block.blockentity;

import net.minecraft.block.entity.BlockEntityType;
import org.antarcticgardens.wave.codegen.CodeGenUtils;
import org.antarcticgardens.wave.codegen.FileGeneratorUtils;
import org.antarcticgardens.wave.codegen.JavaClassFileBuilder;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.antarcticgardens.wave.codegen.elements.declarations.FieldDeclarationElement;
import org.antarcticgardens.wave.codegen.elements.declarations.MethodDeclarationElement;
import org.antarcticgardens.wave.datagen.WaveDataGen;

import java.io.File;
import java.io.IOException;

public class BlockEntityTypesFileGenerator {
    public static final MethodDeclarationElement METHOD = FileGeneratorUtils.createDefaultRegisterMethodDeclaration();
    public static final JavaClassFileBuilder BUILDER = FileGeneratorUtils.createDefaultClassBuilder("BlockEntityTypes", METHOD);

    public static final FieldDeclarationElement REGISTRY =
            CodeGenUtils.createWrappedRegistryField(BlockEntityType.class, true, WaveDataGen.modID, "BLOCK_ENTITY_TYPE", BUILDER.getReferenceContext());

    public static void setup() {
        BUILDER.addElement(-1, REGISTRY);

        WaveDataGen.INIT_METHOD.addElement(0, new MethodCallElement(BUILDER.getSimpleName(), "register"));

        WaveDataGen.runAfterDone(() -> {
            try {
                BUILDER.writeToFile(new File(WaveDataGen.DIRECTORY, "java"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, WaveDataGen.Priority.SAVING_FILES);
    }
}
