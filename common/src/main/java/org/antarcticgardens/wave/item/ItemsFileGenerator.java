package org.antarcticgardens.wave.item;

import net.minecraft.item.Item;
import org.antarcticgardens.wave.codegen.CodeGenUtils;
import org.antarcticgardens.wave.codegen.FileGeneratorUtils;
import org.antarcticgardens.wave.codegen.JavaClassFileBuilder;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.antarcticgardens.wave.codegen.elements.declarations.FieldDeclarationElement;
import org.antarcticgardens.wave.codegen.elements.declarations.MethodDeclarationElement;
import org.antarcticgardens.wave.datagen.WaveDataGen;

import java.io.File;
import java.io.IOException;

public class ItemsFileGenerator {
    public static final MethodDeclarationElement METHOD = FileGeneratorUtils.createDefaultRegisterMethodDeclaration();
    public static final JavaClassFileBuilder BUILDER = FileGeneratorUtils.createDefaultClassBuilder("Items", METHOD);

    public static final FieldDeclarationElement REGISTRY =
            CodeGenUtils.createWrappedRegistryField(Item.class, false, WaveDataGen.modID, "ITEM", BUILDER.getReferenceContext());

    public static void setup() {
        BUILDER.addElement(-1, REGISTRY);
        
        WaveDataGen.INIT_METHOD.addElement(0, new MethodCallElement(BUILDER.getSimpleName(), "register"));

        WaveDataGen.runAfterDone(() -> {
            try {
                BUILDER.writeToFile(new File(WaveDataGen.DIRECTORY, "java"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, WaveDataGen.Priority.SAVING_FILES);
    }
}
