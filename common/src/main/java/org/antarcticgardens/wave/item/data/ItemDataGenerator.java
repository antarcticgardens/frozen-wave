package org.antarcticgardens.wave.item.data;

import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import org.antarcticgardens.wave.item.WaveItem;

public class ItemDataGenerator {

    public static void generate (Class<?> clazz) {

    }

    public static <T> String getItemID(T object) {
        if (object instanceof Item item) {
            return Registries.ITEM.getId(item).toString();
        } else if (object instanceof Class<?> cls) {
            if (cls.isAnnotationPresent(WaveItem.class)) {
                return cls.getAnnotation(WaveItem.class).value();
            }
        }
        throw new RuntimeException(object + " is not a recognised item!111!!1!!11");
    }

}
