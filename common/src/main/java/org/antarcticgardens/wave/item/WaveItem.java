package org.antarcticgardens.wave.item;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Pair;
import org.antarcticgardens.wave.datagen.annotation.ClassAnnotationProcessor;
import org.antarcticgardens.wave.i18n.DefaultTranslation;
import org.antarcticgardens.wave.datagen.asset.AssetDomain;
import org.antarcticgardens.wave.datagen.asset.Model;
import org.antarcticgardens.wave.datagen.asset.TextureMap;
import org.antarcticgardens.wave.group.Group;
import org.antarcticgardens.wave.group.GroupHolder;
import org.antarcticgardens.wave.block.BlocksFileGenerator;
import org.antarcticgardens.wave.codegen.reference.ReferenceUtils;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.antarcticgardens.wave.codegen.elements.declarations.FieldDeclarationElement;
import org.antarcticgardens.wave.codegen.reference.ReferenceContext;
import org.antarcticgardens.wave.util.WaveCachedSupplier;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.i18n.TranslationHelper;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.slf4j.Logger;

import javax.lang.model.element.Modifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WaveItem {
    String value();

    class Processor implements ClassAnnotationProcessor<WaveItem> {
        private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
        private static final Map<String, Class<?>> items = new HashMap<>();

        static {
            ItemsFileGenerator.setup();
            WaveDataGen.runAfterDone(Processor::postProcess, WaveDataGen.Priority.ITEM);
        }

        private static void postProcess() {
            List<Pair<Pair<String, Class<?>>, Set<Group>>> nearProcessed = new ArrayList<>();
            for (Map.Entry<String, Class<?>> in : items.entrySet()) {
                nearProcessed.add(new Pair<>(new Pair<>(in.getKey(), in.getValue()), GroupHolder.getGroups(in.getValue())));
            }

            List<Pair<String, Class<?>>> sortedItems = new SortingTree(nearProcessed).flatten();

            for (Pair<String, Class<?>> pair : sortedItems) {
                String id = pair.getLeft();
                Class<?> aClass = pair.getRight();

                ReferenceContext ref = ItemsFileGenerator.BUILDER.getReferenceContext();
                String settings = ReferenceUtils.instantiate(Item.Settings.class, ref, false);

                if (Block.class.isAssignableFrom(aClass)) {
                    generateItemCode(id, BlockItem.class, ReferenceUtils.format("() -> new {0}({1}.{2}.get(), {3})", ref, 
                            BlockItem.class, BlocksFileGenerator.BUILDER, id.toUpperCase(), settings));
                } else {
                    generateItemCode(id, aClass, "() -> " + ReferenceUtils.instantiate(aClass, ref, false, settings));

                    TranslationHelper.addKey("item." + id, String.format("item.%s.%s", WaveDataGen.modID, id));

                    DefaultTranslation defaultTranslation = aClass.getAnnotation(DefaultTranslation.class);
                    if (defaultTranslation != null) {
                        TranslationHelper.addDefault("item." + id, defaultTranslation.value());
                    }
                    
                    // TODO: Check custom model annotation
                    
                    Model.getOrCreate(id, "minecraft:item/generated", AssetDomain.ITEM)
                            .textures(new TextureMap(id, AssetDomain.ITEM)
                                    .processTextureAnnotations(aClass))
                            .generate();
                }
            }
        }
        
        private static void generateItemCode(String id, Class<?> aClass, String supplier) {
            ReferenceContext ref = ItemsFileGenerator.BUILDER.getReferenceContext();

            FieldDeclarationElement itemField = new FieldDeclarationElement(ref.getReference(WaveCachedSupplier.class), id.toUpperCase())
                    .addModifiers(Modifier.PROTECTED, Modifier.STATIC, Modifier.FINAL)
                    .addGenerics(ref.getReference(aClass))
                    .setValue(ReferenceUtils.instantiate(WaveCachedSupplier.class, ref, true, supplier));

            MethodCallElement registerCall = new MethodCallElement(ItemsFileGenerator.REGISTRY.getName(), "register")
                    .addArguments("\"" + id + "\"", id.toUpperCase());

            MethodCallElement tabCall = new MethodCallElement(ref.getReference(WaveDataGen.GENERAL_STARTER) + ".itemsInTab", "add")
                    .addArguments(id.toUpperCase());

            ItemsFileGenerator.BUILDER.addElement(0, itemField);
            ItemsFileGenerator.METHOD.addElement(0, registerCall);
            ItemsFileGenerator.METHOD.addElement(0, tabCall);
        }
        
        public static void addItem(String id, Class<?> aClass) {
            if (items.containsKey(id)) {
                LOGGER.error("Trying to add item {} with id {} while it is already taken by {}",
                        aClass.getName(), id, items.get(id).getName());
                return;
            }

            items.put(id, aClass);
        }

        @Override
        public void process(Class<?> aClass, WaveItem annotation) {
            if (!Item.class.isAssignableFrom(aClass)) {
                LOGGER.error("Class {} annotated with @WaveItem does not extend Item class", aClass.getName());
                return;
            }
            
            addItem(annotation.value(), aClass);
        }
    }
}
