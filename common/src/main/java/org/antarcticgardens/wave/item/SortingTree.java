package org.antarcticgardens.wave.item;

import net.minecraft.util.Pair;
import org.antarcticgardens.wave.group.Group;

import java.util.*;

public class SortingTree {
    public List<Pair<String, Class<?>>> items = new ArrayList<>();

    public List<SortingTree> branches;

    public SortingTree(List<Pair<Pair<String, Class<?>>, Set<Group>>> list) {
        Map<Group, List<Pair<Pair<String, Class<?>>, Set<Group>>>> groups = new HashMap<>();

        for (Pair<Pair<String, Class<?>>, Set<Group>> item : list) {
            Group bigGroup = null;
            if (list.size() > 1) {
                for (Group group : item.getRight()) {
                    if (group.getPriority() > 0 && (bigGroup == null || group.getPriority() > bigGroup.getPriority())) {
                        bigGroup = group;
                    }
                }
            }

            if (bigGroup != null) {
                item.getRight().remove(bigGroup);
                groups.computeIfAbsent(bigGroup, (a) -> new ArrayList<>()).add(item);
            } else {
                items.add(item.getLeft());
            }
        }
        var sort = groups.entrySet().stream()
                .map((group) -> new Pair<>(group.getKey(), group.getValue()))
                .sorted((a, b) -> Float.compare(a.getLeft().getPriority(), b.getLeft().getPriority()))
                .map((in) -> new SortingTree(in.getRight()));

        branches = sort.toList();

        items.sort((o1, o2) -> String.CASE_INSENSITIVE_ORDER.compare(o1.getLeft(), o2.getLeft()));

    }

    public List<Pair<String, Class<?>>> flatten() {
        List<Pair<String, Class<?>>> items = new ArrayList<>();
        for (SortingTree branch : branches) {
            items.addAll(branch.flatten());
        }
        items.addAll(this.items);

        return items;
    }


}
