package org.antarcticgardens.wave.registry;

import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.WaveImplementations;
import org.antarcticgardens.wave.network.packet.IS2CPacketHandler;

public abstract class WaveS2CPacketRegistry {
    public static void register(Identifier identifier, IS2CPacketHandler handler) {
        WaveImplementations.get(WaveS2CPacketRegistry.class).registerImpl(identifier, handler);
    }

    public abstract void registerImpl(Identifier identifier, IS2CPacketHandler handler);
}
