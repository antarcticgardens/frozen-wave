package org.antarcticgardens.wave.registry;

import net.minecraft.client.gl.ShaderProgram;
import net.minecraft.client.render.VertexFormat;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.WaveImplementations;

import java.util.function.Consumer;

public abstract class WaveShaderRegistry {
    public static void register(Identifier identifier, VertexFormat vertexFormat, Consumer<ShaderProgram> consumer) {
        WaveImplementations.get(WaveShaderRegistry.class).registerImpl(identifier, vertexFormat, consumer);
    }

    public abstract void registerImpl(Identifier identifier, VertexFormat vertexFormat, Consumer<ShaderProgram> consumer);
    
    public record Entry(Identifier identifier, VertexFormat vertexFormat, Consumer<ShaderProgram> consumer) { }
}
