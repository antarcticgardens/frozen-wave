package org.antarcticgardens.wave.registry;

import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import org.antarcticgardens.wave.WaveImplementations;

public abstract class WaveRegistries {
    public static <T> WaveRegistry<T> wrapMinecraftRegistry(String modId, RegistryKey<? extends Registry<T>> registryKey) {
        return WaveImplementations.get(WaveRegistries.class).wrapMinecraftRegistryImpl(modId, registryKey);
    }
    
    protected abstract <T> WaveRegistry<T> wrapMinecraftRegistryImpl(String modId, RegistryKey<? extends Registry<T>> registryKey);
}
