package org.antarcticgardens.wave.registry;

import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.WaveImplementations;
import org.antarcticgardens.wave.inventory.tab.TabConfiguration;
import org.antarcticgardens.wave.util.WaveCachedSupplier;

import java.util.List;

public abstract class WaveTabRegistry {
    public static void register(Identifier identifier, List<WaveCachedSupplier<? extends Item>> items, TabConfiguration config) {
        WaveImplementations.get(WaveTabRegistry.class).registerImpl(identifier, items, config);
    }

    public abstract void registerImpl(Identifier identifier, List<WaveCachedSupplier<? extends Item>> items, TabConfiguration config);
}
