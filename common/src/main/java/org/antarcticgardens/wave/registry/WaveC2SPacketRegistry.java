package org.antarcticgardens.wave.registry;

import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.WaveImplementations;
import org.antarcticgardens.wave.network.packet.IC2SPacketHandler;

public abstract class WaveC2SPacketRegistry {
    public static void register(Identifier identifier, IC2SPacketHandler handler) {
        WaveImplementations.get(WaveC2SPacketRegistry.class).registerImpl(identifier, handler);
    }

    public abstract void registerImpl(Identifier identifier, IC2SPacketHandler handler);
}
