package org.antarcticgardens.wave.registry;

import java.util.function.Supplier;

public abstract class WaveRegistry<T> {
    public abstract void register(String key, Supplier<? extends T> entry);
    public abstract T get(String key);
}
