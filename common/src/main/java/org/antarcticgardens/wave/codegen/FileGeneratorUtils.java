package org.antarcticgardens.wave.codegen;

import org.antarcticgardens.wave.codegen.elements.declarations.MethodDeclarationElement;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.util.ModIDUtil;

import javax.lang.model.element.Modifier;

public class FileGeneratorUtils {
    public static MethodDeclarationElement createDefaultRegisterMethodDeclaration() {
        return new MethodDeclarationElement("register", "void")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC);
    }
    
    public static JavaClassFileBuilder createDefaultClassBuilder(String suffix, MethodDeclarationElement register) {
        return new JavaClassFileBuilder(WaveDataGen.basePackage, ModIDUtil.toClassName(WaveDataGen.modID) + suffix)
                .addModifiers(Modifier.PUBLIC)
                .addElement(1, register);
    }
}
