package org.antarcticgardens.wave.codegen.elements.declarations;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.Element;

import javax.lang.model.element.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FieldDeclarationElement implements Element {
    private final String type;
    private final String name;
    
    private String value;
    
    private final List<Modifier> modifiers = new ArrayList<>();
    private final List<String> generics = new ArrayList<>();
    
    public FieldDeclarationElement(String type, String name) {
        this.type = type;
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public FieldDeclarationElement setValue(String value) {
        this.value = value;
        return this;
    }

    public FieldDeclarationElement addModifiers(Modifier... modifiers) {
        this.modifiers.addAll(Arrays.stream(modifiers).toList());
        return this;
    }

    public FieldDeclarationElement addGenerics(String... generics) {
        this.generics.addAll(Arrays.stream(generics).toList());
        return this;
    }
    
    @Override
    public void generate(JavaWriter writer) {
        writer.appendSeparated(modifiers, " ").append(type);

        if (!generics.isEmpty()) {
            writer.append("<").appendSeparatedWithoutLastSeparator(generics, ", ").append(">");
        }
        
        writer.append(" ").append(name);
        
        if (value != null) {
            writer.append(" = ").append(value);
        }
        
        writer.append(";").appendNewLine();
    }
}
