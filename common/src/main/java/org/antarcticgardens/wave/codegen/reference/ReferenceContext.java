package org.antarcticgardens.wave.codegen.reference;

import org.antarcticgardens.wave.codegen.JavaClassFileBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReferenceContext {
    private final Map<String, String> importedClasses = new HashMap<>();
    private final String packageName;
    
    public ReferenceContext(String packageName) {
        this.packageName = packageName;
    }
    
    public String getReference(Class<?> clazz) {
        Class<?> outerClass = clazz.isMemberClass() ? clazz.getDeclaringClass() : clazz;

        String name = outerClass.getSimpleName();
        String packageName = outerClass.getPackageName();
        
        if (importedClasses.containsKey(name) && !packageName.equals(importedClasses.get(name))) {
            return clazz.getName().replaceAll("\\$", ".");
        } else {
            importedClasses.put(name, packageName);
            
            if (outerClass.equals(clazz)) {
                return name;
            } else {
                return name + "." + clazz.getSimpleName();
            }
        }
    }
    
    public String getReference(JavaClassFileBuilder builder) {
        if (importedClasses.containsKey(builder.getSimpleName()) && !builder.getPackage().equals(importedClasses.get(builder.getSimpleName()))) {
            return builder.getName();
        } else {
            importedClasses.put(builder.getSimpleName(), builder.getPackage());
            return builder.getSimpleName();
        }
    }
    
    public List<String> getImports() {
        List<String> imports = new ArrayList<>();
        
        for (Map.Entry<String, String> e : importedClasses.entrySet()) {
            if (!e.getValue().equals(packageName)) {
                imports.add(e.getValue() + "." + e.getKey());
            }
        }

        return imports;
    }
}
