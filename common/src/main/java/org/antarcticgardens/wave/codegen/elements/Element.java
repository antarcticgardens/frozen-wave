package org.antarcticgardens.wave.codegen.elements;

import org.antarcticgardens.wave.codegen.JavaWriter;

public interface Element {
    void generate(JavaWriter writer);
}
