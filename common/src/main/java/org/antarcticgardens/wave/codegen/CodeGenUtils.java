package org.antarcticgardens.wave.codegen;

import net.minecraft.registry.RegistryKeys;
import org.antarcticgardens.wave.codegen.elements.declarations.FieldDeclarationElement;
import org.antarcticgardens.wave.codegen.reference.ReferenceContext;
import org.antarcticgardens.wave.codegen.reference.ReferenceUtils;
import org.antarcticgardens.wave.registry.WaveRegistries;
import org.antarcticgardens.wave.registry.WaveRegistry;

import javax.lang.model.element.Modifier;

public class CodeGenUtils {
    public static <T> FieldDeclarationElement createWrappedRegistryField(Class<T> aClass, boolean typeWildcard, String modId, String registry, ReferenceContext ref) {
        // I hope registry names are same in different mappings
        
        return new FieldDeclarationElement(ref.getReference(WaveRegistry.class), "REGISTRY")
                .addModifiers(Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                .addGenerics(ref.getReference(aClass) + (typeWildcard ? "<?>" : ""))
                .setValue(ReferenceUtils.format("{0}.wrapMinecraftRegistry(\"{1}\", {2}.{3})", ref, 
                        WaveRegistries.class, modId, RegistryKeys.class, registry));
    }
}
