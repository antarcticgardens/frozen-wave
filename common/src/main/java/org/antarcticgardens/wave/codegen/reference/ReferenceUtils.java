package org.antarcticgardens.wave.codegen.reference;

import org.antarcticgardens.wave.codegen.JavaClassFileBuilder;

import java.text.MessageFormat;
import java.util.Arrays;

public class ReferenceUtils {
    public static String format(String format, ReferenceContext ref, Object... objects) {
        return MessageFormat.format(format, Arrays.stream(objects)
                .map(object -> {
                    if (object instanceof Class<?> cls) {
                        return ref.getReference(cls);
                    } else if (object instanceof JavaClassFileBuilder builder) {
                        return ref.getReference(builder);
                    }
                    
                    return object;
                }).toArray());
    }
    
    public static String instantiate(Class<?> clazz, ReferenceContext context, boolean generics, String... args) {
        StringBuilder finalArgs = new StringBuilder();
        
        for (int i = 0; i < args.length; i++) {
            finalArgs.append(args[i]);
            
            if (i != args.length - 1) {
                finalArgs.append(", ");
            }
        }
        
        return format("new {0}{1}({2})", context, clazz, generics ? "<>" : "", finalArgs.toString());
    }
    
    public static String methodReference(Class<?> clazz, String method, ReferenceContext context) {
        return context.getReference(clazz) + "::" + method;
    }
}
