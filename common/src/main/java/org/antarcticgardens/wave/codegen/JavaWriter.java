package org.antarcticgardens.wave.codegen;

import java.util.List;

public class JavaWriter {
    private final StringBuilder builder = new StringBuilder();
    private final String tab;

    private boolean lineEmpty = true;
    private int tabulation = 0;
    
    public JavaWriter(String tab) {
        this.tab = tab;
    }
    
    public JavaWriter append(String string) {
        applyTabulationIfLineIsEmpty();
        builder.append(string);
        return this;
    }
    
    public JavaWriter appendSeparatedWithoutLastSeparator(List<?> elements, String separator) {
        appendSeparated(elements, separator);

        if (!elements.isEmpty())
            eraseLast(separator.length());

        return this;
    }
    
    public JavaWriter appendSeparated(List<?> elements, String separator) {
        applyTabulationIfLineIsEmpty();

        for (Object obj : elements)
            append(obj.toString()).append(separator);

        return this;
    }

    public JavaWriter eraseLast(int n) {
        builder.delete(builder.length() - n, builder.length());
        return this;
    }

    public JavaWriter appendNewLine() {
        lineEmpty = true;
        builder.append("\n");
        return this;
    }
    
    public JavaWriter increaseTabulation() {
        tabulation++;
        return this;
    }

    public JavaWriter decreaseTabulation() {
        tabulation--;
        return this;
    }

    private void applyTabulationIfLineIsEmpty() {
        if (lineEmpty) {
            builder.append(tab.repeat(tabulation));
            lineEmpty = false;
        }
    }

    @Override
    public String toString() {
        return builder.toString();
    }
}
