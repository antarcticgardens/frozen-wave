package org.antarcticgardens.wave.codegen.elements.declarations;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.Element;

import javax.lang.model.element.Modifier;
import java.util.*;

public class MethodDeclarationElement implements Element {
    private final String name;
    private final String type;
    
    private final List<Modifier> modifiers = new ArrayList<>();
    private final List<String> generics = new ArrayList<>();
    private final List<String> arguments = new ArrayList<>();

    private final Map<Integer, List<Element>> blocks = new TreeMap<>();
    
    public MethodDeclarationElement(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public MethodDeclarationElement addModifiers(Modifier... modifiers) {
        this.modifiers.addAll(Arrays.stream(modifiers).toList());
        return this;
    }

    public MethodDeclarationElement addGenerics(String... generics) {
        this.generics.addAll(Arrays.stream(generics).toList());
        return this;
    }
    
    public MethodDeclarationElement addArgument(String type, String name) {
        arguments.add(type + " " + name);
        return this;
    }

    public MethodDeclarationElement addElement(int block, Element element) {
        List<Element> blk = blocks.getOrDefault(block, new ArrayList<>());
        blk.add(element);
        blocks.put(block, blk);

        return this;
    }

    public MethodDeclarationElement addElementInFront(int block, Element element) {
        List<Element> blk = blocks.getOrDefault(block, new ArrayList<>());
        blk.add(0, element);
        blocks.put(block, blk);

        return this;
    }
    
    @Override
    public void generate(JavaWriter writer) {
        writer.appendSeparated(modifiers, " ");
        
        if (!generics.isEmpty()) {
            writer.append("<").appendSeparatedWithoutLastSeparator(generics, ", ").append("> ");
        }
        
        writer.append(type).append(" ").append(name)
                .append("(").appendSeparatedWithoutLastSeparator(arguments, ", ").append(") ")
                .append("{").appendNewLine().increaseTabulation();

        boolean first = true;
        for (Map.Entry<Integer, List<Element>> e : blocks.entrySet()) {
            if (!first) {
                writer.appendNewLine();
            }

            for (Element element : e.getValue()) {
                element.generate(writer);
            }

            first = false;
        }
        
        writer.decreaseTabulation().append("}").appendNewLine();
    }
}
