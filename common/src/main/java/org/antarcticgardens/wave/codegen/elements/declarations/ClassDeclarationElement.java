package org.antarcticgardens.wave.codegen.elements.declarations;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.Element;

import javax.lang.model.element.Modifier;
import java.util.*;

public class ClassDeclarationElement implements Element {
    protected final String name;
    
    private final List<Modifier> modifiers = new ArrayList<>();
    private final List<String> generics = new ArrayList<>();
    
    private String extendsClass = null;
    private final List<String> implementsClasses = new ArrayList<>();
    
    private final Map<Integer, List<Element>> blocks = new TreeMap<>();
    
    public ClassDeclarationElement(String name) {
        this.name = name;
    }
    
    public ClassDeclarationElement extend(String clazz) {
        this.extendsClass = clazz;
        return this;
    }
    
    public ClassDeclarationElement addImplements(String... classes) {
        implementsClasses.addAll(Arrays.stream(classes).toList());
        return this;
    }
    
    public ClassDeclarationElement addModifiers(Modifier... modifiers) {
        this.modifiers.addAll(Arrays.stream(modifiers).toList());
        return this;
    }
    
    public ClassDeclarationElement addGenerics(String... generics) {
        this.generics.addAll(Arrays.stream(generics).toList());
        return this;
    }
    
    public ClassDeclarationElement addElement(int block, Element element) {
        List<Element> blk = blocks.getOrDefault(block, new ArrayList<>());
        blk.add(element);
        blocks.put(block, blk);
        
        return this;
    }

    public ClassDeclarationElement addElementInFront(int block, Element element) {
        List<Element> blk = blocks.getOrDefault(block, new ArrayList<>());
        blk.add(0, element);
        blocks.put(block, blk);
        
        return this;
    }
    
    @Override
    public void generate(JavaWriter writer) {
        writer.appendSeparated(modifiers, " ")
                .append("class ").append(name);
        
        if (!generics.isEmpty()) {
            writer.append("<").appendSeparatedWithoutLastSeparator(generics, ", ").append(">");
        }
        
        writer.append(" ");
        
        if (extendsClass != null) {
            writer.append("extends ").append(extendsClass).append(" ");
        }
        
        if (!implementsClasses.isEmpty()) {
            writer.append("implements ").appendSeparatedWithoutLastSeparator(implementsClasses, ", ").append(" ");
        }
        
        writer.append("{").appendNewLine().increaseTabulation();
        
        boolean first = true;
        for (Map.Entry<Integer, List<Element>> e : blocks.entrySet()) {
            if (!first) {
                writer.appendNewLine();
            }
            
            for (Element element : e.getValue()) {
                element.generate(writer);
            }
            
            first = false;
        }
        
        writer.decreaseTabulation().append("}").appendNewLine();
    }
}
