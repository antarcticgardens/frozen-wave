package org.antarcticgardens.wave.codegen.elements.calls;

import org.antarcticgardens.wave.codegen.JavaWriter;
import org.antarcticgardens.wave.codegen.elements.Element;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MethodCallElement implements Element {
    private final String owner;
    private final String name;
    
    private final List<String> args = new ArrayList<>();
    
    public MethodCallElement(String owner, String name) {
        this.owner = owner;
        this.name = name;
    }
    
    public MethodCallElement addArguments(String... args) {
        this.args.addAll(Arrays.stream(args).toList());
        return this;
    }

    @Override
    public void generate(JavaWriter writer) {
        writer.append(owner).append(".").append(name).append("(").appendSeparatedWithoutLastSeparator(args, ", ").append(");").appendNewLine();
    }
}
