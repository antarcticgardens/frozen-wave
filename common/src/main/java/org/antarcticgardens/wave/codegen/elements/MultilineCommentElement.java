package org.antarcticgardens.wave.codegen.elements;

import org.antarcticgardens.wave.codegen.JavaWriter;

import java.util.ArrayList;
import java.util.List;

public class MultilineCommentElement implements Element {
    private final List<String> lines = new ArrayList<>();
    
    public MultilineCommentElement addLine(String line) {
        lines.add(line);
        return this;
    }

    @Override
    public void generate(JavaWriter writer) {
        writer.append("/*").appendNewLine()
                .increaseTabulation();
        
        for (String line : lines) {
            writer.append(line).appendNewLine();
        }
        
        writer.decreaseTabulation()
                .append(" */").appendNewLine();
    }
}
