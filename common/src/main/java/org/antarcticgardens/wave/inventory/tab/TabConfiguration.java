package org.antarcticgardens.wave.inventory.tab;

import net.minecraft.item.ItemStack;
import org.antarcticgardens.wave.group.Group;
import org.jetbrains.annotations.Nullable;

public interface TabConfiguration {

    /**
     * 
     * @param group group if the tab is getting split into sub tabs.
     * @return icon
     */
    ItemStack getIcon(@Nullable Group group);

}
