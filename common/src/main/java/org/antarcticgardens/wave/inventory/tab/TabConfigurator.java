package org.antarcticgardens.wave.inventory.tab;

import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.datagen.annotation.ClassAnnotationProcessor;
import org.antarcticgardens.wave.i18n.DefaultTranslation;
import org.antarcticgardens.wave.codegen.reference.ReferenceUtils;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.antarcticgardens.wave.codegen.elements.declarations.FieldDeclarationElement;
import org.antarcticgardens.wave.codegen.reference.ReferenceContext;
import org.antarcticgardens.wave.util.WaveCachedSupplier;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.i18n.TranslationHelper;
import org.antarcticgardens.wave.registry.WaveTabRegistry;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.slf4j.Logger;

import javax.lang.model.element.Modifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.LinkedList;
import java.util.List;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TabConfigurator {
    class Processor implements ClassAnnotationProcessor<TabConfigurator> {
        private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
        
        @Override
        public void process(Class<?> aClass, TabConfigurator annotation) {
            if (!TabConfiguration.class.isAssignableFrom(aClass)) {
                LOGGER.error("Class {} annotated with @TabConfigurator does not implement TabConfiguration interface", aClass.getName());
                return;
            }

            ReferenceContext ref = WaveDataGen.GENERAL_STARTER.getReferenceContext();
            FieldDeclarationElement itemsInTab = new FieldDeclarationElement(ref.getReference(List.class), "itemsInTab")
                    .addGenerics(ref.getReference(WaveCachedSupplier.class) + "<? extends " + ref.getReference(Item.class) + ">")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .setValue(ReferenceUtils.instantiate(LinkedList.class, ref, true));

            WaveDataGen.GENERAL_STARTER.addElement(0, itemsInTab);

            WaveDataGen.runAfterDone(() -> {
                MethodCallElement call = new MethodCallElement(ref.getReference(WaveTabRegistry.class), "register")
                        .addArguments(
                                ReferenceUtils.format("new {0}(\"{1}:tab\")", ref, Identifier.class, WaveDataGen.modID),
                                "itemsInTab",
                                ReferenceUtils.instantiate(aClass, ref, false)
                        );

                WaveDataGen.INIT_METHOD.addElement(0, call);
            }, WaveDataGen.Priority.TAB);

            TranslationHelper.addKey("itemGroup", "itemGroup." + WaveDataGen.modID + ".tab");
            if (aClass.isAnnotationPresent(DefaultTranslation.class)) {
                TranslationHelper.addDefault("itemGroup", aClass.getAnnotation(DefaultTranslation.class).value());
            }
        }
    }
}
