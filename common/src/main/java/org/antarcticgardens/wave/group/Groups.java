package org.antarcticgardens.wave.group;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Groups {
    Class<? extends GroupHolder>[] value();
}
