package org.antarcticgardens.wave.group;

public class Group {
    private float priority = 0;

    public float getPriority() {
        return priority;
    }

    public Group setPriority(float priority) {
        this.priority = priority;
        return this;
    }
}
