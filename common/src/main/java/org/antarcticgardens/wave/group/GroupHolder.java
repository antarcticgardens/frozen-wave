package org.antarcticgardens.wave.group;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

public interface GroupHolder {
    Group get();

    static Set<Group> getGroups(Class<?> clazz) {
        Groups groups = clazz.getAnnotation(Groups.class);
        if (groups != null) {
            Set<Group> gr = new HashSet<>(groups.value().length);
            for (Class<? extends GroupHolder> aClass : groups.value()) {
                try {
                    gr.add(aClass.getConstructor().newInstance().get());
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                         NoSuchMethodException e) {
                    throw new RuntimeException(e);
                }
            }
            return gr;
        }
        return new HashSet<>();
    }

}
