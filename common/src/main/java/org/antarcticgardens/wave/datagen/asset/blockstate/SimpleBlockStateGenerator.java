package org.antarcticgardens.wave.datagen.asset.blockstate;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import org.antarcticgardens.wave.asset.model.ModelParent;
import org.antarcticgardens.wave.block.WaveBlock;
import org.antarcticgardens.wave.block.WaveBlockAssetGenerator;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.datagen.asset.AssetDomain;
import org.antarcticgardens.wave.datagen.asset.Model;
import org.antarcticgardens.wave.datagen.asset.TextureMap;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.slf4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class SimpleBlockStateGenerator {
    private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
    
    private static void postProcess() {
        for (Map.Entry<String, Class<?>> e : WaveBlock.Processor.getBlocks().entrySet()) {
            String id = e.getKey();
            Class<?> aClass = e.getValue();

            if (!WaveBlockAssetGenerator.Processor.isAssetGeneratorPresent(id)) {
                ModelParent modelParent = aClass.getAnnotation(ModelParent.class);

                if (modelParent != null) { 
                    try {
                        Constructor<?> blockConstructor = aClass.getConstructor(AbstractBlock.Settings.class);
                        Block block = (Block) blockConstructor.newInstance(AbstractBlock.Settings.create());

                        Model blockModel = Model.getOrCreate(id, modelParent.value(), AssetDomain.BLOCK)
                                .textures(new TextureMap(id, AssetDomain.BLOCK)
                                        .processTextureAnnotations(aClass)
                                        .texture("particle", "#up"));

                        new VariantBlockStateGenerator(id, block, state -> new ModelEntry[] {
                                new ModelEntry(blockModel)
                        }).generate();

                        Model.getOrCreate(id, blockModel.getPath(), AssetDomain.ITEM).generate();
                    } catch (InvocationTargetException | NoSuchMethodException | InstantiationException |
                             IllegalAccessException ex) {
                        throw new RuntimeException(ex);
                    }
                } else {
                    LOGGER.error("@{} is not specified for {}", ModelParent.class.getSimpleName(), aClass.getName());
                }
            }
        }
    }
    
    public static void setup() {
        WaveDataGen.runAfterDone(SimpleBlockStateGenerator::postProcess, WaveDataGen.Priority.SIMPLE_BLOCK_STATES);
    }
}
