package org.antarcticgardens.wave.datagen.utils;

import net.minecraft.server.MinecraftServer;
import org.antarcticgardens.wave.datagen.WaveDataGen;

import java.io.*;
import java.util.*;

public class WaveDataGenFileUtils {

    public static Set<File> recursiveList(File folder) {
        HashSet<File> set = new HashSet<>();
        if (folder.isDirectory()) {
            for (File file : folder.listFiles()) {
                set.addAll(recursiveList(file));
            }
            return set;
        }
        return Set.of(folder);
    }

    public static Set<Class<?>> bakeClassListFromPackage(File root) {
        return bakeClassListFromPackage(root, "");
    }

    public static Set<Class<?>> bakeClassListFromPackage(File root, String path) {
        HashSet<Class<?>> set = new HashSet<>();
        if (root.isDirectory()) {
            for (File file : root.listFiles()) {
                set.addAll(bakeClassListFromPackage(file, path + (path.isEmpty() ? "" : ".") + file.getName()));
            }
            return set;
        }

        try {
            if (path.endsWith(".java")) {
                Class<?> clazz = Class.forName(path.substring(0, path.lastIndexOf(".")), false, MinecraftServer.class.getClassLoader());
                set.add(clazz);
                return set;
            }
            return Set.of();
        } catch (Exception e) {
            WaveDataGen.LOGGER.info("", e);
            return Set.of();
        }
    }
}
