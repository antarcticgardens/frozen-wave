package org.antarcticgardens.wave.datagen.asset;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

public class Model {
    private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
    private static final Map<AssetDomain, Map<String, Model>> domains = new HashMap<>();
    
    private final String name;
    private final AssetDomain domain;
    private final String parent;
    
    private TextureMap textures = null;
    
    private boolean generated = false;
    
    private Model(String name, String parent, AssetDomain domain) { 
        this.name = name;
        this.domain = domain;
        this.parent = parent;
    }
    
    public static Model getOrCreate(String name, String parent, AssetDomain domain) {
        Map<String, Model> domainModels = domains.getOrDefault(domain, new HashMap<>());
        Model model = domainModels.get(name);
        
        if (model != null) {
            if (!parent.equals(model.parent)) {
                LOGGER.error("Trying to create model {} with parent {} but there's already a model with the same name inherited from {}", name, parent, model.parent);
            }
        } else {
            model = new Model(name, parent, domain);
            domainModels.put(name, model);
        }

        return model;
    }
    
    public Model textures(TextureMap textures) {
        this.textures = textures;
        return this;
    }
    
    public Model generate() {
        if (generated) {
            return this;
        }
        
        JsonObject root = new JsonObject();
        root.addProperty("parent", resolveParent());
        
        if (textures != null) {
            Map<String, String> textures = this.textures.getTextures();

            if (!textures.isEmpty()) {
                JsonObject texturesObject = new JsonObject();

                for (Map.Entry<String, String> e : textures.entrySet()) {
                    texturesObject.addProperty(e.getKey(), e.getValue());
                }

                root.add("textures", texturesObject);
            }
        }

        File output = new File(getModelDirectory(), domain + "/" + name + ".json");
        output.getParentFile().mkdirs();

        try (FileWriter writer = new FileWriter(output)) {
            new GsonBuilder()
                    .setPrettyPrinting()
                    .create()
                    .toJson(root, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        generated = true;
        return this;
    }
    
    public String getPath() {
        return WaveDataGen.modID + ":" + domain + "/" + name;
    }
    
    private String resolveParent() {
        if (!parent.contains(":")) {
            File from = new File(WaveDataGen.ROOT, "assets/" + parent + ".json");
            String result = domain + "/" + FilenameUtils.getName(parent) + "_parent";

            if (!from.exists()) {
                LOGGER.error("Parent model {} of {} does not exist", parent, name);
                return result;
            }

            File to = new File(getModelDirectory(), result + ".json");
            to.getParentFile().mkdirs();

            try {
                Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            return WaveDataGen.modID + ":" + result;
        } else {
            return parent;
        }
    }

    private File getModelDirectory() {
        return new File(WaveDataGen.DIRECTORY, "resources/assets/" + WaveDataGen.modID + "/models/");
    }
}
