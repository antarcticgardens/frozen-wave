package org.antarcticgardens.wave.datagen.asset.blockstate;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.data.client.When;
import org.antarcticgardens.wave.datagen.asset.blockstate.ModelEntry;

public class MultipartEntry {
    private final When when;
    private final ModelEntry[] apply;
    
    public MultipartEntry(When when, ModelEntry[] apply) {
        this.when = when;
        this.apply = apply;
    }

    public MultipartEntry(When when, ModelEntry apply) {
        this.when = when;
        this.apply = new ModelEntry[] { apply };
    }

    public MultipartEntry(ModelEntry[] apply) {
        this.when = null;
        this.apply = apply;
    }

    public MultipartEntry(ModelEntry apply) {
        this.when = null;
        this.apply = new ModelEntry[] { apply };
    }
    
    public JsonObject generate() {
        JsonObject object = new JsonObject();
        
        if (when != null) {
            object.add("when", when.get());
        }
        
        if (apply.length == 1) {
            object.add("apply", apply[0].generate());
        } else {
            JsonArray array = new JsonArray();

            for (ModelEntry modelEntry : apply) {
                array.add(modelEntry.generate());
            }

            object.add("apply", array);
        }
        
        return object;
    }
}
