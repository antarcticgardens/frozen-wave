package org.antarcticgardens.wave.datagen.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public interface ClassAnnotationProcessor<T extends Annotation> {
    default void process(Class<?> clazz, T annotation) {}
    default void process(Method method, T annotation) {}
    default void process(Field field, T annotation) {}
}
