package org.antarcticgardens.wave.datagen.annotation;

import org.antarcticgardens.wave.block.WaveBlock;
import org.antarcticgardens.wave.block.blockentity.WaveBlockEntity;
import org.antarcticgardens.wave.block.WaveBlockAssetGenerator;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.item.WaveItem;
import org.antarcticgardens.wave.asset.CopyAsset;
import org.antarcticgardens.wave.network.packet.WaveC2SPacketHandler;
import org.antarcticgardens.wave.network.packet.WaveS2CPacketHandler;
import org.antarcticgardens.wave.annotation.other.CallStatic;
import org.antarcticgardens.wave.inventory.tab.TabConfigurator;
import org.antarcticgardens.wave.datagen.utils.WaveDataGenFileUtils;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

public class ProcessAnnotationsStep {
    private static final Map<Class<?>, ClassAnnotationProcessor> annotationProcessors = Map.of(
            WaveBlock.class, new WaveBlock.Processor(),
            WaveBlockAssetGenerator.class, new WaveBlockAssetGenerator.Processor(),
            WaveBlockEntity.class, new WaveBlockEntity.Processor(),
            
            WaveItem.class, new WaveItem.Processor(),
            TabConfigurator.class, new TabConfigurator.Processor(),
            WaveS2CPacketHandler.class, new WaveS2CPacketHandler.Processor(),
            WaveC2SPacketHandler.class, new WaveC2SPacketHandler.Processor(),
            CopyAsset.class, new CopyAsset.Processor(),
            CallStatic.class, new CallStatic.Processor()
    );

    public static void process() {
        if (WaveDataGen.sourceDirectory == null) {
            throw new RuntimeException("Source directory isn't set");
        }

        Set<Class<?>> classes = WaveDataGenFileUtils.bakeClassListFromPackage(new File(WaveDataGen.ROOT, WaveDataGen.sourceDirectory));

        for (Class<?> aClass : classes) {
            processClass(aClass);
        }
    }
    
    private static void processClass(Class<?> aClass) {
        for (Annotation annotation : aClass.getAnnotations()) {
            var processor = annotationProcessors.get(annotation.annotationType());
            if (processor != null)
                processor.process(aClass, annotation);
        }

        for (Field declaredField : aClass.getDeclaredFields()) {
            for (Annotation annotation : declaredField.getAnnotations()) {
                var processor = annotationProcessors.get(annotation.annotationType());
                if (processor != null)
                    processor.process(declaredField, annotation);
            }
        }

        for (Method declaredMethod : aClass.getDeclaredMethods()) {
            for (Annotation annotation : declaredMethod.getAnnotations()) {
                var processor = annotationProcessors.get(annotation.annotationType());
                if (processor != null)
                    processor.process(declaredMethod, annotation);
            }
        }

        for (Class<?> declaredClass : aClass.getDeclaredClasses()) {
            processClass(declaredClass);
        }
    }
}
