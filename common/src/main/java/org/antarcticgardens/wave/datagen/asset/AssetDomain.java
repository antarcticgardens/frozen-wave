package org.antarcticgardens.wave.datagen.asset;

public enum AssetDomain {
    ITEM,
    BLOCK;
    
    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
