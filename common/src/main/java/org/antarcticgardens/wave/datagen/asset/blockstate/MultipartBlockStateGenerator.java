package org.antarcticgardens.wave.datagen.asset.blockstate;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.property.Property;
import org.antarcticgardens.wave.datagen.asset.BlockAssetGenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MultipartBlockStateGenerator {
    private final List<MultipartEntry> entries = new ArrayList<>();
    private final String id;

    public MultipartBlockStateGenerator(String id) {
        this.id = id;
    }
    
    public MultipartBlockStateGenerator addEntry(MultipartEntry entry) {
        entries.add(entry);
        return this;
    }

    public MultipartBlockStateGenerator generate() {
        JsonObject root = new JsonObject();
        JsonArray multipart = new JsonArray();
        
        for (MultipartEntry entry : entries) {
            multipart.add(entry.generate());
        }

        root.add("multipart", multipart);

        File output = new File(BlockAssetGenerator.BLOCKSTATE_DIRECTORY, id + ".json");
        output.getParentFile().mkdirs();

        try (FileWriter writer = new FileWriter(output)) {
            new GsonBuilder()
                    .setPrettyPrinting()
                    .create()
                        .toJson(root, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return this;
    }
}
