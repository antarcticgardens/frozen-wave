package org.antarcticgardens.wave.datagen;

import org.antarcticgardens.wave.codegen.JavaClassFileBuilder;
import org.antarcticgardens.wave.codegen.elements.declarations.MethodDeclarationElement;
import org.antarcticgardens.wave.datagen.annotation.ProcessAnnotationsStep;
import org.antarcticgardens.wave.util.ModIDUtil;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.slf4j.Logger;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class WaveDataGen {
    private static final Map<Priority, List<Runnable>> postDoneFunctions = new TreeMap<>();

    // final static parameters
    public static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
    public static final File ROOT = new File(System.getProperty("wave.datagen.root"));

    // configuration parameters
    public static String sourceDirectory = null;
    public static String outputDirectory = null;
    public static String basePackage;
    public static String modID;

    // different variable parameters
    public static MethodDeclarationElement INIT_METHOD;
    public static JavaClassFileBuilder GENERAL_STARTER;

    public static MethodDeclarationElement CLIENT_INIT_METHOD;
    public static JavaClassFileBuilder CLIENT_STARTER;

    public static File DIRECTORY;

    public static void process() {
        LOGGER.info("Beginning data generation!");

        // TODO: We should probably pass all this *mandatory* shit as arguments?
        //region Boilerplate checks
        if (outputDirectory == null) {
            throw new RuntimeException("Output directory isn't set");
        }

        if (basePackage == null) {
            throw new RuntimeException("Base package isn't set");
        }

        if (modID == null) {
            throw new RuntimeException("Mod ID isn't set");
        }
        //endregion

        DIRECTORY = new File(ROOT, outputDirectory);
        DIRECTORY.mkdirs();

        INIT_METHOD = new MethodDeclarationElement("initialize", "void")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC);

        GENERAL_STARTER = new JavaClassFileBuilder(basePackage, ModIDUtil.toClassName(modID) + "Generated")
                        .addModifiers(Modifier.PUBLIC)
                        .addElement(1, INIT_METHOD);

        CLIENT_INIT_METHOD = new MethodDeclarationElement("initializeClient", "void")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC);

        CLIENT_STARTER = new JavaClassFileBuilder(basePackage + ".client", ModIDUtil.toClassName(modID) + "ClientGenerated")
                        .addModifiers(Modifier.PUBLIC)
                        .addElement(1, CLIENT_INIT_METHOD);

        ProcessAnnotationsStep.process();

        for (List<Runnable> functions : postDoneFunctions.values()) {
            for (Runnable runnable : functions) {
                runnable.run();
            }
        }

        try {
            GENERAL_STARTER.writeToFile(new File(DIRECTORY, "java"));
            CLIENT_STARTER.writeToFile(new File(DIRECTORY, "java"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void runAfterDone(Runnable function, Priority priority) {
        List<Runnable> functions = postDoneFunctions.getOrDefault(priority, new ArrayList<>());
        functions.add(function);
        postDoneFunctions.put(priority, functions);
    }
    
    public enum Priority {
        BLOCK,
        BLOCK_ASSETS,
        SIMPLE_BLOCK_STATES,
        BLOCK_ENTITY,
        ITEM,
        TAB,
        TRANSLATION,
        SAVING_FILES;
    }
}
