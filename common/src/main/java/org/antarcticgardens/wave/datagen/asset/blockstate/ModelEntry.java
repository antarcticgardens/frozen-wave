package org.antarcticgardens.wave.datagen.asset.blockstate;

import com.google.gson.JsonObject;
import org.antarcticgardens.wave.datagen.asset.Model;

public class ModelEntry {
    private final JsonObject data = new JsonObject();
    private final Model model;
    
    public ModelEntry(Model model) {
        this.model = model;
    }

    public ModelEntry setX(int x) {
        data.addProperty("x", x);
        return this;
    }

    public ModelEntry setY(int y) {
        data.addProperty("y", y);
        return this;
    }
    
    public ModelEntry setUVLock(boolean uvLock) {
        data.addProperty("uvlock", uvLock);
        return this;
    }
    
    public ModelEntry setWeight(int weight) {
        data.addProperty("weight", weight);
        return this;
    }
    
    protected JsonObject generate() {
        data.addProperty("model", model.generate().getPath());
        return data;
    }
}
