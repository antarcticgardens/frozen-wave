package org.antarcticgardens.wave.datagen.asset;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.antarcticgardens.wave.Wave;
import org.antarcticgardens.wave.asset.texture.Texture;
import org.antarcticgardens.wave.asset.texture.Textures;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class TextureMap {
    private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
    
    private final Map<String, String> textures = new HashMap<>();
    private final String owner;
    private final AssetDomain domain;
    
    public TextureMap(String owner, AssetDomain domain) {
        this.owner = owner;
        this.domain = domain;
    }
    
    public TextureMap texture(String id, String path) {
        if (!path.contains(":") && !path.startsWith("#")) {
            File png = new File(WaveDataGen.ROOT, "assets/" + path + ".png");
            String flatPath = FilenameUtils.getName(path);

            if (!flatPath.startsWith(owner)) {
                flatPath = owner + "_" + flatPath;
            }

            flatPath = domain + "/" + flatPath;

            if (png.exists()) {
                textures.put(id, generatePngTexture(png, flatPath));
            } else {
                File gif = new File(WaveDataGen.ROOT, "assets/" + path + ".gif");

                if (gif.exists()) {
                    textures.put(id, generateGifTexture(gif, flatPath));
                } else {
                    LOGGER.error("Couldn't find texture file for {}", path);
                }
            }
        } else {
            textures.put(id, path);
        }
        
        return this;
    }
    
    public TextureMap textures(String path) {
        File directory = new File(WaveDataGen.ROOT, "assets/" + path);
        
        if (!directory.isDirectory()) {
            LOGGER.error("{} is not a directory path", path);
            return this;
        }
        
        for (File file : directory.listFiles()) {
            String name = FilenameUtils.removeExtension(file.getName());
            String flatPath = FilenameUtils.getName(name);

            if (!flatPath.startsWith(owner)) {
                flatPath = owner + "_" + flatPath;
            }

            flatPath = domain + "/" + flatPath;

            if (file.isDirectory()) {
                // TODO: Do something (another way of generating animated textures maybe?)
            } else if (file.getName().endsWith(".png")) {
                textures.put(name, generatePngTexture(file, flatPath));
            } else if (file.getName().endsWith(".gif")) {
                textures.put(name, generateGifTexture(file, flatPath));
            } else if (file.getName().endsWith(".json")) {
                continue;
            } else {
                LOGGER.error("Unable to generate texture {}! Only PNG and GIF are supported", flatPath);
            }
        }
        
        return this;
    }
    
    public TextureMap processTextureAnnotations(Class<?> aClass) {
        Textures textures = aClass.getAnnotation(Textures.class);
        
        if (textures != null) {
            textures(textures.value());
        }
        
        for (Texture texture : aClass.getAnnotationsByType(Texture.class)) {
            texture(texture.id(), texture.value());
        }
        
        return this;
    }

    private String generatePngTexture(File file, String path) {
        File output = new File(getTextureDirectory(), path + ".png");
        output.getParentFile().mkdirs();

        try {
            Files.copy(file.toPath(), output.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return WaveDataGen.modID + ":" + path;
    }

    private String generateGifTexture(File file, String path) {
        File output = new File(getTextureDirectory(), path + ".png");

        try {
            if (output.lastModified() < file.lastModified()) {
                output.getParentFile().mkdirs();

                ImageReader reader = ImageIO.getImageReadersByFormatName("gif").next();
                ImageInputStream stream = ImageIO.createImageInputStream(file);
                reader.setInput(stream);

                int count = reader.getNumImages(true);

                BufferedImage image = new BufferedImage(reader.getWidth(0), reader.getHeight(0) * count, BufferedImage.TYPE_INT_ARGB);

                var g = image.getGraphics();
                for (int index = 0; index < count; index++) {
                    BufferedImage frame = reader.read(index);
                    g.drawImage(frame, 0, index * frame.getHeight(), null);
                }

                ImageIO.write(image, "png", output);
            }

            int frametime = -1;
            File data = new File(file.getParentFile(), file.getName() + ".json");

            if (data.exists()) {
                JsonObject root = JsonParser.parseReader(new FileReader(data)).getAsJsonObject();
                JsonElement frametimeElement = root.get("frametime");

                if (frametimeElement != null) {
                    frametime = frametimeElement.getAsInt();
                }
            }

            if (frametime <= 0) {
                LOGGER.warn("Frame time for texture {} is not specified or is specified incorrectly. Defaulting to 1", path);
                frametime = 1;
            }

            generateMcmeta(path, frametime);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return WaveDataGen.modID + ":" + path;
    }

    private void generateMcmeta(String path, int frametime) {
        JsonObject root = new JsonObject();
        JsonObject animation = new JsonObject();
        animation.addProperty("frametime", frametime);
        root.add("animation", animation);

        File output = new File(getTextureDirectory(), path + ".png.mcmeta");
        output.getParentFile().mkdirs();

        try (FileWriter writer = new FileWriter(output)) {
            new GsonBuilder()
                    .setPrettyPrinting()
                    .create()
                    .toJson(root, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private File getTextureDirectory() {
        return new File(WaveDataGen.DIRECTORY, "resources/assets/" + WaveDataGen.modID + "/textures/");
    }
    
    public Map<String, String> getTextures() {
        return Collections.unmodifiableMap(textures);
    }
}
