package org.antarcticgardens.wave.datagen.asset.blockstate;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.state.property.Property;
import org.antarcticgardens.wave.datagen.asset.BlockAssetGenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.function.Function;
import java.util.stream.Collectors;

public class VariantBlockStateGenerator {
    private final String id;
    private final Block block;
    private final Function<BlockState, ModelEntry[]> variantHandler;
    
    public VariantBlockStateGenerator(String id, Block block, Function<BlockState, ModelEntry[]> variantHandler) {
        this.id = id;
        this.block = block;
        this.variantHandler = variantHandler;
    }
    
    public VariantBlockStateGenerator generate() {
        JsonObject root = new JsonObject();
        JsonObject variants = new JsonObject();

        for (BlockState state : block.getStateManager().getStates()) {
            String stateName = state.getEntries().entrySet().stream()
                    .map(e -> e.getKey().getName() + "=" + ((Property) e.getKey()).name(e.getValue()))
                    .collect(Collectors.joining(","));

            ModelEntry[] modelEntries = variantHandler.apply(state);

            if (modelEntries.length == 1) {
                variants.add(stateName, modelEntries[0].generate());
            } else {
                JsonArray array = new JsonArray();

                for (ModelEntry modelEntry : modelEntries) {
                    array.add(modelEntry.generate());
                }

                variants.add(stateName, array);
            }
        }

        root.add("variants", variants);

        File output = new File(BlockAssetGenerator.BLOCKSTATE_DIRECTORY, id + ".json");
        output.getParentFile().mkdirs();

        try (FileWriter writer = new FileWriter(output)) {
            new GsonBuilder()
                    .setPrettyPrinting()
                    .create()
                        .toJson(root, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
        return this;
    }
}
