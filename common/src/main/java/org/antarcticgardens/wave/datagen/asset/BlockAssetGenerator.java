package org.antarcticgardens.wave.datagen.asset;

import net.minecraft.block.Block;
import org.antarcticgardens.wave.datagen.WaveDataGen;

import java.io.File;

public interface BlockAssetGenerator {
    File BLOCKSTATE_DIRECTORY = new File(WaveDataGen.DIRECTORY, "resources/assets/" + WaveDataGen.modID + "/blockstates/");
    
    void generateAssets(Block block);
}
