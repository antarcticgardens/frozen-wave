package org.antarcticgardens.wave;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Wave {
	public static final String MOD_ID = "wave";
	private static final Logger LOGGER = LoggerFactory.getLogger("Wave");

	public static void init() {
		LOGGER.info("Wave breaks in!");
	}
}
