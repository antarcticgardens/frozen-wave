package org.antarcticgardens.wave.annotation.other;

import org.antarcticgardens.wave.datagen.annotation.ClassAnnotationProcessor;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.antarcticgardens.wave.datagen.WaveDataGen;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CallStatic {
    CallPosition value();

    class Processor implements ClassAnnotationProcessor<CallStatic> {
        @Override
        public void process(Method method, CallStatic annotation) {
            MethodCallElement call = new MethodCallElement(annotation.value().getReferenceContext().getReference(method.getDeclaringClass()), method.getName());

            switch (annotation.value()) {
                case FIRST_CLIENT -> WaveDataGen.CLIENT_INIT_METHOD.addElementInFront(0, call);
                case LAST_CLIENT -> WaveDataGen.CLIENT_INIT_METHOD.addElement(0, call);
                case FIRST_COMMON -> WaveDataGen.INIT_METHOD.addElementInFront(0, call);
                case LAST_COMMON -> WaveDataGen.INIT_METHOD.addElement(0, call);
            }
        }
    }
}
