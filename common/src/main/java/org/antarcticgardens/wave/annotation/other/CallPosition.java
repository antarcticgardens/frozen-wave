package org.antarcticgardens.wave.annotation.other;

import org.antarcticgardens.wave.codegen.reference.ReferenceContext;
import org.antarcticgardens.wave.datagen.WaveDataGen;

public enum CallPosition {
    FIRST_CLIENT(WaveDataGen.CLIENT_STARTER.getReferenceContext()),
    LAST_CLIENT(WaveDataGen.CLIENT_STARTER.getReferenceContext()),
    FIRST_COMMON(WaveDataGen.GENERAL_STARTER.getReferenceContext()),
    LAST_COMMON(WaveDataGen.GENERAL_STARTER.getReferenceContext());
    
    private final ReferenceContext referenceContext;

    CallPosition(ReferenceContext referenceContext) {
        this.referenceContext = referenceContext;
    }

    public ReferenceContext getReferenceContext() {
        return referenceContext;
    }
}
