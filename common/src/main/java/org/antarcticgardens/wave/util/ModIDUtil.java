package org.antarcticgardens.wave.util;

public class ModIDUtil {

    public static String toClassName(String id) {
        StringBuilder str = new StringBuilder();
        boolean nextCapital = true;
        for (int i = 0 ; i < id.length() ; i++) {
            char c = id.charAt(i);
            if (c == '_') {
                nextCapital = true;
            } else {
                if (nextCapital) {
                    nextCapital = false;
                    c = Character.toUpperCase(c);
                }
                str.append(c);
            }
        }

        return str.toString();
    }

}
