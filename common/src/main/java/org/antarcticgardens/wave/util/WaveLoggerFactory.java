package org.antarcticgardens.wave.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WaveLoggerFactory {
    private static final StackWalker STACK_WALKER = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE);
    
    public static Logger getClassLogger() {
        String name = STACK_WALKER.getCallerClass().getSimpleName()
                .replaceAll("\\$", "/");
        
        if (name.startsWith("Wave")) {
            name = name.replaceFirst("Wave", "");
        } 
        
        return LoggerFactory.getLogger("Wave/" + name);
    }
}
