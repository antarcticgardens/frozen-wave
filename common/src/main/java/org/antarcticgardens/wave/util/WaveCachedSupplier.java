package org.antarcticgardens.wave.util;

import java.util.function.Supplier;

public final class WaveCachedSupplier<T> implements Supplier<T> {
    private final Supplier<T> supplier;
    private T object;

    public WaveCachedSupplier(Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public Supplier<T> getSupplier() {
        return () -> {
            if (object == null) {
                object = supplier.get();
            }
            
            return object;
        };
    }

    @Override
    public T get() {
        return getSupplier().get();
    }
}
