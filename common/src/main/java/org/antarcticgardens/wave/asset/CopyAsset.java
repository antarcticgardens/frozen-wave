package org.antarcticgardens.wave.asset;

import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.datagen.annotation.ClassAnnotationProcessor;
import org.antarcticgardens.wave.datagen.WaveDataGen;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@Retention(RetentionPolicy.RUNTIME)
public @interface CopyAsset {
    String value();

    class Processor implements ClassAnnotationProcessor<CopyAsset> {
        @Override
        public void process(Field field, CopyAsset annotation) {
            if (field.getType() == Identifier.class) {
                try {
                    Identifier id = (Identifier) field.get(null);
                    String outer = "";
                    if (field.isAnnotationPresent(CopyAssetTo.class)) {
                        outer = field.getAnnotation(CopyAssetTo.class).value();
                    }

                    File f = new File(WaveDataGen.DIRECTORY, "resources/assets/"  + id.getNamespace() + "/" + outer + id.getPath());

                    File in = new File(new File(WaveDataGen.ROOT, "assets/"), annotation.value());

                    f.getParentFile().mkdirs();
                    Files.copy(in.toPath(), f.toPath(), StandardCopyOption.REPLACE_EXISTING);

                } catch (IllegalAccessException | IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
