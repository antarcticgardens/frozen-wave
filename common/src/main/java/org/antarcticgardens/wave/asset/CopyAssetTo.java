package org.antarcticgardens.wave.asset;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface CopyAssetTo {
    String value();
}
