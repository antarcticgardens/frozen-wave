package org.antarcticgardens.wave.asset.texture;

import java.lang.annotation.*;

@Repeatable(Texture.Container.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Texture {
    String id() default "layer0"; 
    String value();

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @interface Container {
        Texture[] value();
    }
}
