package org.antarcticgardens.wave.asset.texture;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Textures {
    String value();
}
