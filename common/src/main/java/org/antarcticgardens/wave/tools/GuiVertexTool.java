package org.antarcticgardens.wave.tools;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.render.*;
import net.minecraft.util.Identifier;
import org.joml.Math;
import org.joml.Matrix4f;

public class GuiVertexTool {

    public static void drawLine(DrawContext context, int x, int y, int x2, int y2, int halfWidth, float r, float g, float b, float a, RenderLayer layer) {

        float theta =
                (float) (Math.atan2(x - x2, y - y2) +
                        Math.PI * 0.5);

        float sin = Math.sin(theta);
        float cos = Math.cos(theta);

        VertexConsumer consumer = context.getVertexConsumers().getBuffer(layer);


        consumer.color(r, g, b, a);
        consumer.vertex(x + halfWidth * sin, y + halfWidth * cos, 0).color(r, g, b, a).next();
        consumer.vertex(x2 + halfWidth * sin, y2 + halfWidth * cos, 0).color(r, g, b, a).next();
        consumer.vertex(x2 - halfWidth * sin, y2 - halfWidth * cos, 0).color(r, g, b, a).next();
        consumer.vertex(x - halfWidth * sin, y - halfWidth * cos, 0).color(r, g, b, a).next();

        context.draw();

    }

    public static void drawLine(DrawContext context, int x, int y, int x2, int y2, int halfWidth, float r, float g, float b, float a) {
        drawLine(context, x, y, x2, y2, halfWidth, r, g, b, a, RenderLayer.getGui());
    }


    /**
     * Bring your own shader
     */
    public static void drawTexture(DrawContext context, Identifier texture, int x, int y, int u, int v, int width, int height) {
        drawTexture(context, texture, x, y, 0, (float)u, (float)v, width, height, 256, 256);
    }

    /**
     * Bring your own shader
     */
    public static void drawTexture(DrawContext context, Identifier texture, int x, int y, int z, float u, float v, int width, int height, int textureWidth, int textureHeight) {
        drawTexture(context, texture, x, x + width, y, y + height, z, width, height, u, v, textureWidth, textureHeight);
    }

    /**
     * Bring your own shader
     */
    public static void drawTexture(DrawContext context, Identifier texture, int x, int y, int width, int height, float u, float v, int regionWidth, int regionHeight, int textureWidth, int textureHeight) {
        drawTexture(context, texture, x, x + width, y, y + height, 0, regionWidth, regionHeight, u, v, textureWidth, textureHeight);
    }

    /**
     * Bring your own shader
     */
    public static void drawTexture(DrawContext context, Identifier texture, int x, int y, float u, float v, int width, int height, int textureWidth, int textureHeight) {
        drawTexture(context, texture, x, y, width, height, u, v, width, height, textureWidth, textureHeight);
    }

    /**
     * Bring your own shader
     */
    public static void drawTexture(DrawContext context, Identifier texture, int x1, int x2, int y1, int y2, int z, int regionWidth, int regionHeight, float u, float v, int textureWidth, int textureHeight) {
        drawTexturedQuad(context, texture, x1, x2, y1, y2, z, (u + 0.0F) / (float)textureWidth, (u + (float)regionWidth) / (float)textureWidth, (v + 0.0F) / (float)textureHeight, (v + (float)regionHeight) / (float)textureHeight);
    }

    /**
     * Bring your own shader
     */
    public static void drawTexturedQuad(DrawContext context, Identifier texture, int x1, int x2, int y1, int y2, int z, float u1, float u2, float v1, float v2) {
        RenderSystem.setShaderTexture(0, texture);

        Matrix4f matrix4f = context.getMatrices().peek().getPositionMatrix();
        BufferBuilder bufferBuilder = Tessellator.getInstance().getBuffer();

        bufferBuilder.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_TEXTURE);

        bufferBuilder.vertex(matrix4f, (float)x1, (float)y1, (float)z).texture(u1, v1).next();
        bufferBuilder.vertex(matrix4f, (float)x1, (float)y2, (float)z).texture(u1, v2).next();
        bufferBuilder.vertex(matrix4f, (float)x2, (float)y2, (float)z).texture(u2, v2).next();
        bufferBuilder.vertex(matrix4f, (float)x2, (float)y1, (float)z).texture(u2, v1).next();

        BufferRenderer.drawWithGlobalProgram(bufferBuilder.end());
    }

    /**
     * Bring your own shader
     */
    public static void drawQuad(DrawContext context, int x1, int y1, int x2, int y2, int z) {

        Matrix4f matrix4f = context.getMatrices().peek().getPositionMatrix();
        BufferBuilder bufferBuilder = Tessellator.getInstance().getBuffer();

        bufferBuilder.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION);

        bufferBuilder.vertex(matrix4f, (float)x1, (float)x2, (float)z).next();
        bufferBuilder.vertex(matrix4f, (float)x1, (float)y2, (float)z).next();
        bufferBuilder.vertex(matrix4f, (float)y1, (float)y2, (float)z).next();
        bufferBuilder.vertex(matrix4f, (float)y1, (float)x2, (float)z).next();

        BufferRenderer.drawWithGlobalProgram(bufferBuilder.end());
    }


}
