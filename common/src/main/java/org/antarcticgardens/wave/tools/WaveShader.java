package org.antarcticgardens.wave.tools;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gl.ShaderProgram;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.render.*;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.registry.WaveShaderRegistry;
import org.joml.Matrix4f;

public class WaveShader {
    private ShaderProgram shader;

    public WaveShader(Identifier identifier, VertexFormat format) {
        WaveShaderRegistry.register(identifier, format, (s) -> shader = s);
        // TODO: ability to toggle off custom shaders
    }

    public void uniform(String name, float[] value) {
        shader.getUniform(name).set(value); // TODO: safety? maybe?
    }

    public ShaderProgram get() {
        return shader;
    }

    public void bind() {
        RenderSystem.setShader(this::get);
    }

    // TODO: draw not here?
    
    public void drawFlatTexture(DrawContext context, Identifier texture, int x, int y, int width, int height) {
        drawFlatTexture(context, texture, x, y, 0, width, height);
    }

    public void drawFlatTexture(DrawContext context, Identifier texture, int x, int y, int z, int width, int height) {
        drawFlatTexturedQuad(context, texture, x, x + width, y, y + height, z, 0, 0, width, height);
    }

    public void drawFlatTexturedQuad(DrawContext context, Identifier texture, int x1, int x2, int y1, int y2, int z, float u1, float u2, float v1, float v2) {
        RenderSystem.setShaderTexture(0, texture);
        bind();

        Matrix4f matrix4f = context.getMatrices().peek().getPositionMatrix();
        BufferBuilder bufferBuilder = Tessellator.getInstance().getBuffer();

        bufferBuilder.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_TEXTURE);

        bufferBuilder.vertex(matrix4f, (float)x1, (float)y1, (float)z).texture(u1, v1).next();
        bufferBuilder.vertex(matrix4f, (float)x1, (float)y2, (float)z).texture(u1, v2).next();
        bufferBuilder.vertex(matrix4f, (float)x2, (float)y2, (float)z).texture(u2, v2).next();
        bufferBuilder.vertex(matrix4f, (float)x2, (float)y1, (float)z).texture(u2, v1).next();

        BufferRenderer.drawWithGlobalProgram(bufferBuilder.end());


    }

    public void drawColouredRectangle(DrawContext context, float r, float g, float b, float a, int x, int y, int width, int height) {
        drawColouredQuad(context, x, x + width, y, y + width, 0, r, g, b, a);
    }

    public void drawColouredQuad(DrawContext context, int x1, int x2, int y1, int y2, int z, float r, float g, float b, float a) {
        bind();

        Matrix4f matrix4f = context.getMatrices().peek().getPositionMatrix();
        BufferBuilder bufferBuilder = Tessellator.getInstance().getBuffer();

        bufferBuilder.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_COLOR);

        bufferBuilder.vertex(matrix4f, (float)x1, (float)y1, (float)z).color(r, g, b, a).next();
        bufferBuilder.vertex(matrix4f, (float)x1, (float)y2, (float)z).color(r, g, b, a).next();
        bufferBuilder.vertex(matrix4f, (float)x2, (float)y2, (float)z).color(r, g, b, a).next();
        bufferBuilder.vertex(matrix4f, (float)x2, (float)y1, (float)z).color(r, g, b, a).next();

        BufferRenderer.drawWithGlobalProgram(bufferBuilder.end());
    }



}
