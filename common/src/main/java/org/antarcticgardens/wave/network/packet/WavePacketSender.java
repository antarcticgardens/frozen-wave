package org.antarcticgardens.wave.network.packet;

import io.netty.buffer.Unpooled;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.WaveImplementations;

public abstract class WavePacketSender {
    public static final PacketByteBuf EMPTY = new PacketByteBuf(Unpooled.EMPTY_BUFFER);
    
    public static void sendS2C(ServerPlayerEntity player, Identifier identifier, PacketByteBuf packet) {
        WaveImplementations.get(WavePacketSender.class).sendS2CImpl(player, identifier, packet);
    }

    public static void sendC2S(Identifier identifier, PacketByteBuf packet) {
        WaveImplementations.get(WavePacketSender.class).sendC2SImpl(identifier, packet);
    }
    
    public abstract void sendS2CImpl(ServerPlayerEntity player, Identifier identifier, PacketByteBuf packet);
    public abstract void sendC2SImpl(Identifier identifier, PacketByteBuf packet);
}
