package org.antarcticgardens.wave.network.packet;

import net.minecraft.util.Identifier;
import org.antarcticgardens.wave.datagen.annotation.ClassAnnotationProcessor;
import org.antarcticgardens.wave.codegen.reference.ReferenceUtils;
import org.antarcticgardens.wave.codegen.elements.calls.MethodCallElement;
import org.antarcticgardens.wave.codegen.reference.ReferenceContext;
import org.antarcticgardens.wave.datagen.WaveDataGen;
import org.antarcticgardens.wave.registry.WaveC2SPacketRegistry;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface WaveC2SPacketHandler {
    String value();

    class Processor implements ClassAnnotationProcessor<WaveC2SPacketHandler> {
        @Override
        public void process(Method method, WaveC2SPacketHandler annotation) {
            ReferenceContext ref = WaveDataGen.GENERAL_STARTER.getReferenceContext();
            MethodCallElement call = new MethodCallElement(ref.getReference(WaveC2SPacketRegistry.class), "register")
                    .addArguments(
                            ReferenceUtils.format("new {0}(\"{1}:{2}\")", ref, 
                                    Identifier.class, WaveDataGen.modID, annotation.value()),
                            ReferenceUtils.methodReference(method.getDeclaringClass(), method.getName(), ref)
                    );

            WaveDataGen.INIT_METHOD.addElement(0, call);
        }
    }
}
