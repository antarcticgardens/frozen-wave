package org.antarcticgardens.wave.network.packet;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;

public interface IC2SPacketHandler {
    void handle(ServerPlayerEntity player, PacketByteBuf buf);
}
