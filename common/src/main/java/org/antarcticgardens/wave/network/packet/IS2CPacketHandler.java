package org.antarcticgardens.wave.network.packet;

import net.minecraft.network.PacketByteBuf;

public interface IS2CPacketHandler {
    void handle(PacketByteBuf buf);
}
