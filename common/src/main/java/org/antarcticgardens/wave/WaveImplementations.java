package org.antarcticgardens.wave;

import org.antarcticgardens.wave.util.WaveLoggerFactory;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class WaveImplementations {
    private static final Logger LOGGER = WaveLoggerFactory.getClassLogger();
    private static final Map<Class<?>, Object> implementations = new HashMap<>();

    public static <T> void put(Class<T> clazz, T implementation) {
        implementations.put(clazz, implementation);
    }
    
    public static void put(Map<Class<?>, Object> implementations) {
        for (Map.Entry<Class<?>, Object> e : implementations.entrySet()) {
            if (!e.getKey().isAssignableFrom(e.getValue().getClass())) {
                LOGGER.error("Class {} can not be an implementation of {}", 
                        e.getValue().getClass().getName(), e.getKey().getName());
                continue;
            }

            WaveImplementations.implementations.put(e.getKey(), e.getValue());
        }
    }
    
    public static <T> T get(Class<T> clazz) {
        return clazz.cast(implementations.get(clazz));
    }
}
